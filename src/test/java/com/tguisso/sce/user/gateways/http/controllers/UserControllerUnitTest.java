package com.tguisso.sce.user.gateways.http.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.http.controllers.json.UserJson;
import com.tguisso.sce.user.usecases.DeleteUserUseCase;
import com.tguisso.sce.user.usecases.GetUserUseCase;
import com.tguisso.sce.user.usecases.SaveUserUseCase;

@ExtendWith(MockitoExtension.class)
public class UserControllerUnitTest {
	
	@InjectMocks
	private UserController userController;
	
	@Mock
	private SaveUserUseCase saveUserUseCase;
	
	@Mock
	private GetUserUseCase getUserUseCase;
	
	@Mock
	private DeleteUserUseCase deleteUserUseCase;
	
	@Test
	public void createUserWithSuccess() {
		String idToBeReturn = "anyId";
		UserJson userJson = new UserJson(null, "anyEmail", "anyName", "anyPassword");
		
		doReturn(idToBeReturn).when(saveUserUseCase).save(any(User.class));
		
		String idReturned = userController.create(userJson);
		
		ArgumentCaptor<User> userAC = ArgumentCaptor.forClass(User.class);
		verify(saveUserUseCase).save(userAC.capture());

		assertEquals(idToBeReturn, idReturned);
		
		User userReturned = userAC.getValue();
		
		assertNull(userReturned.getId());
		assertEquals(userJson.getEmail(), userReturned.getEmail());
		assertEquals(userJson.getName(), userReturned.getName());
		assertEquals(userJson.getPassword(), userReturned.getPassword());
	}
	
	@Test
	public void updateUserWithSuccess() {
		UserJson userJson = new UserJson("anyId", "anyEmail", "anyName", "anyPassword");

		userController.update(userJson);
		
		ArgumentCaptor<User> userAC = ArgumentCaptor.forClass(User.class);
		verify(saveUserUseCase).save(userAC.capture());

		User userReturned = userAC.getValue();
		
		assertEquals(userJson.getId(), userReturned.getId());
		assertEquals(userJson.getEmail(), userReturned.getEmail());
		assertEquals(userJson.getName(), userReturned.getName());
		assertEquals(userJson.getPassword(), userReturned.getPassword());
	}
	
	@Test
	public void getUserById() {
		String id = "anyId";
		User user = new User("anyId", "anyEmail", "anyName", null);
		doReturn(user).when(getUserUseCase).get(id);
		
		UserJson userJsonReturned = userController.get(id);
		
		verify(getUserUseCase).get(id);
		
		assertEquals(id, userJsonReturned.getId());
		assertEquals(user.getId(), userJsonReturned.getId());
		assertEquals(user.getEmail(), userJsonReturned.getEmail());
		assertEquals(user.getName(), userJsonReturned.getName());
		assertEquals(user.getPassword(), userJsonReturned.getPassword());
	}
	
	@Test
	public void getUserByEmail() {
		String email = "anyEmail";

		User user = new User("anyId", "anyEmail", "anyName", null);
		doReturn(user).when(getUserUseCase).getByEmail(email);
		
		UserJson userJsonReturned = userController.getByEmail(email);
		
		verify(getUserUseCase).getByEmail(email);
		
		assertEquals(user.getId(), userJsonReturned.getId());
		assertEquals(email, userJsonReturned.getEmail());
		assertEquals(user.getName(), userJsonReturned.getName());
		assertNull(userJsonReturned.getPassword());
	}
	
	@Test
	public void deleteUser() {
		String id = "anyId";
		String userName = "anyName";
		userController.delete(id, userName);
		verify(deleteUserUseCase).delete(id, userName);
	}
}
