package com.tguisso.sce.user.gateways.database.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class GatewayExceptionUnitTest {
	
	@Test
	public void errorToAccessDatabaseGatewayException() {
		SceBaseException exception = new ErrorToAccessDatabaseGatewayException();
		assertEquals("sce.errorToAccessDatabase", exception.getCode());
		assertEquals("Error to access database", exception.getMessage());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
	}

}
