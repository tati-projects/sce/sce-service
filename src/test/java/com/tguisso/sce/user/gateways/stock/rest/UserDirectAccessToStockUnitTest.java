package com.tguisso.sce.user.gateways.stock.rest;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.gateway.http.controllers.StockController;
import com.tguisso.sce.user.gateways.stock.exception.ErrorToAccessStockServiceGatewayException;

@ExtendWith(MockitoExtension.class)
public class UserDirectAccessToStockUnitTest {
	
	@InjectMocks
	private UserDirectAccessToStock userDirectAccessToStock;
	
	@Mock
	private StockController stockController;
		
	@Test
	public void deleteStockByUserId() {
		final String userId = "anyId";
		final String userName = "anyName";
		
		userDirectAccessToStock.deleteStockByUserId(userId, userName);
		verify(stockController).deleteByUser(userId, userName);
	}
	
	@Test
	public void deleteStockByUserIdErrorToAccessStockServiceGatewayException() {
		final String userId = "anyId";
		final String userName = "anyName";
		
		doThrow(new RuntimeException()).when(stockController).deleteByUser(userId, userName);
		
		assertThrows(ErrorToAccessStockServiceGatewayException.class, () -> {
			userDirectAccessToStock.deleteStockByUserId(userId, userName);
		});
		verify(stockController).deleteByUser(userId, userName);
	}

}
