package com.tguisso.sce.user.gateways.product.rest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doThrow;

import com.tguisso.sce.product.gateway.http.controllers.ProductController;
import com.tguisso.sce.user.gateways.product.exception.ErrorToAccessProductServiceGatewayException;

@ExtendWith(MockitoExtension.class)
public class UserDirectAccessToProductUnitTest {
	
	@InjectMocks
	private UserDirectAccessToProduct userDirectAccessToProduct;
	
	@Mock
	private ProductController productController;
	
	@Test
	public void deleteProductByUserID() {
		final String userId = "anyId";
		final String userName = "anyName";
		
		userDirectAccessToProduct.deleteProductByUserId(userId, userName);
		
		verify(productController).deleteByUser(userId, userName);
	}
	
	@Test
	public void deleteProductByUserIDErrorToAccessProductServiceGatewayException() {
		final String userId = "anyId";
		final String userName = "anyName";
		
		doThrow(new RuntimeException()).when(productController).deleteByUser(userId, userName);
			
		assertThrows(ErrorToAccessProductServiceGatewayException.class, () -> {
			userDirectAccessToProduct.deleteProductByUserId(userId, userName);
		});
		
		verify(productController).deleteByUser(userId, userName);
	}

}
