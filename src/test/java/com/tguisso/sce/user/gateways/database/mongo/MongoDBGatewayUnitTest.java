package com.tguisso.sce.user.gateways.database.mongo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.gateways.database.exception.ErrorToAccessDatabaseGatewayException;

@ExtendWith(MockitoExtension.class)
public class MongoDBGatewayUnitTest {
	
	@InjectMocks
	private UserDBGateway mongoDBGateway = new MongoDBGateway();
	
	@Mock
	private UserRepository userRepository;
	
	@Test
	public void findByEmailUserExistent() {
		User user = new User("anyId", "anyEmail", "anyName", "anyPassword");
		
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userRepository).findByEmail(user.getEmail());
		
		Optional<User> userOptional = mongoDBGateway.findByEmail(user.getEmail());
		verify(userRepository).findByEmail(user.getEmail());
		
		assertFalse(userOptional.isEmpty());
		assertEquals(user.getEmail(), userOptional.get().getEmail());
	}
	
	@Test
	public void findByEmailUserNotExistent() {
		User user = new User("anyId", "anyEmail", "anyName", "anyPassword");
		
		Optional<User> userOp = Optional.empty();
		doReturn(userOp).when(userRepository).findByEmail(user.getEmail());
		
		Optional<User> userOptional = mongoDBGateway.findByEmail(user.getEmail());
		verify(userRepository).findByEmail(user.getEmail());
		
		assertTrue(userOptional.isEmpty());
	}
	
	@Test
	public void findByEmailErrorToAccessDBGateway() {
		String email = "anyEmail";
		doThrow(new RuntimeException("anyMessage")).when(userRepository).findByEmail(email);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			mongoDBGateway.findByEmail(email);
		});
		
		verify(userRepository).findByEmail(email);
	}
	
	@Test
	public void findById() {
		String id = "anyId";
		User user = new User("anyId", null, null, null);
		
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userRepository).findById(id);
		
		Optional<User> userReturned = mongoDBGateway.findById(id);
		
		verify(userRepository).findById(id);
		
		assertEquals(id, userReturned.get().getId());
		assertFalse(userReturned.isEmpty());
	}
	
	@Test
	public void findByIdErrorToAccessDBGatewayException() {
		String id = "anyId";
		doThrow(new RuntimeException()).when(userRepository).findById(id);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			mongoDBGateway.findById(id);
		});
		
		verify(userRepository).findById(id);
	}
	
	@Test
	public void saveUserWithSuccess() {
		User user = new User("anyID", "anyEmail", "anyName", "anyPassword");
		
		doReturn(user).when(userRepository).save(user);
		
		String idReturned = mongoDBGateway.save(user);
		
		verify(userRepository).save(user);
		
		assertEquals(user.getId(), idReturned);
		
	}
	
	@Test
	public void saveUserError() {
		User user = new User("anyID", "anyEmail", "anyName", "anyPassword");
		
		doThrow(new RuntimeException("anyMessage")).when(userRepository).save(user);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			mongoDBGateway.save(user);
		});
		
		verify(userRepository).save(user);
	}
	
	@Test
	public void deleteById() {
		String id = "anyId";
		mongoDBGateway.deleteById(id);
		verify(userRepository).deleteById(id);
	}
	
	@Test
	public void deleteByIdException() {
		String id = "anyId";
		
		doThrow(new RuntimeException()).when(userRepository).deleteById(id);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			mongoDBGateway.deleteById(id);
		});
		
		verify(userRepository).deleteById(id);
	}
}
