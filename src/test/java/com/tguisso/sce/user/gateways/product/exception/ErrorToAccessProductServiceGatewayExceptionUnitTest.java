package com.tguisso.sce.user.gateways.product.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class ErrorToAccessProductServiceGatewayExceptionUnitTest {
	
	@Test
	public void errorToAccessProductServiceGatewayException() {
		SceBaseException exception = new ErrorToAccessProductServiceGatewayException();
		assertEquals("sce.user.product.errorToAccessProductService", exception.getCode());
		assertEquals("Error to access product service", exception.getMessage());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
	}

}
