package com.tguisso.sce.user.usecases;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.usecases.exception.UserUnauthorizedBusinessException;

@ExtendWith(MockitoExtension.class)
public class UserAuthorizationUseCaseUnitTest {
	
	@InjectMocks
	private UserAuthorizationUseCase userAuthorizationUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Test
	public void authorize() {
		String id = "anyId";
		String email = "anyEmail";
		User userToAuthorize = new User("anyId", null, null, null);
		
		Optional<User> userOpAuthorized = Optional.of(userToAuthorize);
		doReturn(userOpAuthorized).when(userDBGateway).findByEmail(email);
		
		userAuthorizationUseCase.authorize(id, email);
		
		verify(userDBGateway).findByEmail(email);
	}
	
	@Test
	public void unauthorizedUserNotFound() {
		String id = "anyId";
		String email = "anyEmail";
		
		Optional<User> userEmpty = Optional.empty();
		doReturn(userEmpty).when(userDBGateway).findByEmail(email);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			userAuthorizationUseCase.authorize(id, email);
		});
		
		verify(userDBGateway).findByEmail(email);
	}
	
	@Test
	public void unauthorizedAnotherUser() {
		String id = "anyId";
		String email = "anyEmail";
		User userToAuthorize = new User("otherId", null, null, null);
		
		Optional<User> userOpAuthorized = Optional.of(userToAuthorize);
		doReturn(userOpAuthorized).when(userDBGateway).findByEmail(email);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			userAuthorizationUseCase.authorize(id, email);
		});
		
		verify(userDBGateway).findByEmail(email);
	}

}
