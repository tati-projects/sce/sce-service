package com.tguisso.sce.user.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;

@ExtendWith(MockitoExtension.class)
public class SaveUserUseCaseUnitTest {
	
	@InjectMocks
	private SaveUserUseCase saveUserUseCase;
	
	@Mock
	private SaveUserValidateUseCase saveUserValidateUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Test
	public void testSaveUserCreateSuccess() {
		String idToBeReturn = "anyId";
		User user = new User(null, "anyEmail", "anyName", "anyPassword");
		
		doReturn(idToBeReturn).when(userDBGateway).save(user);
		
		String idReturned = saveUserUseCase.save(user);
		
		verify(saveUserValidateUseCase).validate(user);
		verify(userDBGateway).save(user);
		
		assertEquals(idToBeReturn, idReturned);
	}

}
