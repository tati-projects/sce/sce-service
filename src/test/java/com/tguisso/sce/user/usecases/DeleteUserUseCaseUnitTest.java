package com.tguisso.sce.user.usecases;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.gateways.product.UserProductGateway;
import com.tguisso.sce.user.gateways.stock.StockGateway;

@ExtendWith(MockitoExtension.class)
public class DeleteUserUseCaseUnitTest {
	
	@InjectMocks
	private DeleteUserUseCase deleteUserUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Mock
	private StockGateway stockGateway;
	
	@Mock
	private UserProductGateway userProductGateway;
	
	@Mock
	private UserAuthorizationUseCase userAuthorizationUseCase;
	
	@Test
	public void delete() {
		String id = "anyId";
		String userName = "anyUserName";
		
		deleteUserUseCase.delete(id, userName);
		
		verify(userAuthorizationUseCase).authorize(id, userName);
		verify(userProductGateway).deleteProductByUserId(id, userName);
		verify(stockGateway).deleteStockByUserId(id, userName);
		verify(userDBGateway).deleteById(id);
	}

}
