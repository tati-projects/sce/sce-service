package com.tguisso.sce.user.usecases;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.usecases.exception.UserAlreadyExistsBusinessException;

@ExtendWith(MockitoExtension.class)
public class SaveUserValidateUseCaseUnitTest {
	
	@InjectMocks
	private SaveUserValidateUseCase saveUserValidateUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Test
	public void createNewUserWithSuccess() {
		User user = new User(null, "anyEmail", "anyName", "anyPassword");
		
		saveUserValidateUseCase.validate(user);
		
		verify(userDBGateway).findByEmail(user.getEmail());
	}
	
	@Test
	public void dontCreateWhenUserAlreadyExists() {
		User user = new User(null, "anyEmail", "anyName", "anyPassword");
		User userExistent = new User("anyId", "anyEmail", "anyOtherName", "anyOtherPassword");
		
		Optional<User> userOptional = Optional.of(userExistent);
		doReturn(userOptional).when(userDBGateway).findByEmail(user.getEmail());
		
		assertThrows(UserAlreadyExistsBusinessException.class, () -> {
			saveUserValidateUseCase.validate(user);
		});
		
		verify(userDBGateway).findByEmail(user.getEmail());
		
	}
	
	@Test
	public void updatedUserWithSuccess() {
		User user = new User("anyId", "anyEmail", "anyName", "anyPassword");
		User userExistent = new User("anyId", "anyEmail", "anyName", "anyPassword");
		
		Optional<User> userOptional = Optional.of(userExistent);
		doReturn(userOptional).when(userDBGateway).findByEmail(user.getEmail());
		
		saveUserValidateUseCase.validate(user);
		
		verify(userDBGateway).findByEmail(user.getEmail());
		
	}
	
	@Test
	public void donUpdateWhenUserAlreadyExists() {
		User user = new User("anyId", "anyEmail", "anyName", "anyPassword"); 
		User userExistent = new User("anyOtherId", "anyEmail", "anyOtherName", "anyOtherPassword");
		
		Optional<User> userOptional = Optional.of(userExistent);
		doReturn(userOptional).when(userDBGateway).findByEmail(user.getEmail());
		
		assertThrows(UserAlreadyExistsBusinessException.class, () -> {
			saveUserValidateUseCase.validate(user);
		});
		
		verify(userDBGateway).findByEmail(user.getEmail());
		
	}
	
}
