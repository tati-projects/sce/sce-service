package com.tguisso.sce.user.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.usecases.exception.UserNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class GetUserUseCaseUnitTest {
	
	@InjectMocks
	private GetUserUseCase getUserUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Test
	public void get() {
		String id = "anyId";
		User userToReturn = new User("anyId", "anyEmail", "anyName", null);
		
		Optional<User> userOp = Optional.of(userToReturn);
		doReturn(userOp).when(userDBGateway).findById(id);
		
		User userReturned = getUserUseCase.get(id);
		
		verify(userDBGateway).findById(id);
		
		assertEquals(userToReturn.getId(), userReturned.getId());
		assertEquals(userToReturn.getEmail(), userReturned.getEmail());
		assertEquals(userToReturn.getName(), userReturned.getName());
		assertNull(userReturned.getPassword());
	}
	
	@Test
	public void userNotFoundBusinessException() {
		String id = "anyId";
		Optional<User> userOp = Optional.empty();
		doReturn(userOp).when(userDBGateway).findById(id);
		
		assertThrows(UserNotFoundBusinessException.class, () -> {
			getUserUseCase.get(id);
		});
		
		verify(userDBGateway).findById(id);
	}
	
	@Test
	public void getUserByEmail() {
		String email = "anyEmail";
		User userToReturn = new User("anyId", "anyEmail", "anyName", null);
		
		Optional<User> userOp = Optional.of(userToReturn);
		doReturn(userOp).when(userDBGateway).findByEmail(email);
		
		User userReturned = getUserUseCase.getByEmail(email);
		
		verify(userDBGateway).findByEmail(email);
		
		assertEquals(userToReturn.getId(), userReturned.getId());
		assertEquals(userToReturn.getEmail(), userReturned.getEmail());
		assertEquals(userToReturn.getName(), userReturned.getName());
		assertNull(userReturned.getPassword());
	}
	
	@Test
	public void getUserByEmailNotFoundBusinessException() {
		String email = "anyEmail";
		Optional<User> userOp = Optional.empty();
		doReturn(userOp).when(userDBGateway).findByEmail(email);
		
		assertThrows(UserNotFoundBusinessException.class, () -> {
			getUserUseCase.getByEmail(email);
		});
		
		verify(userDBGateway).findByEmail(email);
	}

}
