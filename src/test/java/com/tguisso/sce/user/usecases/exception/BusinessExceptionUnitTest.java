package com.tguisso.sce.user.usecases.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class BusinessExceptionUnitTest {
	
	@Test
	public void userAlreadyExistsBusinessException() {
		SceBaseException exception = new UserAlreadyExistsBusinessException();
		assertEquals("sce.userAlreadyExists", exception.getCode());
		assertEquals("User already exists", exception.getMessage());
		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
	}
	
	@Test
	public void userNotFoundBusinessException() {
		SceBaseException exception = new UserNotFoundBusinessException();
		assertEquals("sce.user.userNotFound", exception.getCode());
		assertEquals("User not found", exception.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
	}
	
	@Test
	public void userUnauthorizedBusinessException() {
		SceBaseException exception = new UserUnauthorizedBusinessException();
		assertEquals("sce.userUnauthorized", exception.getCode());
		assertEquals("User unauthorized", exception.getMessage());
		assertEquals(HttpStatus.UNAUTHORIZED, exception.getHttpStatus());
	}

}
