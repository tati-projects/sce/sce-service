package com.tguisso.sce.stock.gateway.product.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class ErrorToAccessProductGatewayExceptionUnitTest {
	
	@Test
	public void errorToAccessProductGatewayException() {
		SceBaseException exception = new ErrorToAccessProductGatewayException();
		assertEquals("sce.stock.product.errorToAccessProductService", exception.getCode());
		assertEquals("Error to access product service", exception.getMessage());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
	}
}
