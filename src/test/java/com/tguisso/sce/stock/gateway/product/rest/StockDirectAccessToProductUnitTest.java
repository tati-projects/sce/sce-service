package com.tguisso.sce.stock.gateway.product.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.gateway.http.controllers.ProductController;
import com.tguisso.sce.product.gateway.http.controllers.json.ProductJson;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.gateway.product.exception.ErrorToAccessProductGatewayException;

@ExtendWith(MockitoExtension.class)
public class StockDirectAccessToProductUnitTest {
	
	@InjectMocks
	StockDirectAccessToProduct stockDirectAccessToProduct;
	
	@Mock
	private ProductController productController;
	
	@Test
	public void get() {
		String userName = "anyEmail";
		String productId = "anyId";
		
		List<ProductJson> productsJson = new ArrayList<>();
		productsJson.add(new ProductJson(productId, null, null, null, null));
		doReturn(productsJson).when(productController).getAll(userName);
		
		List<Product> products = stockDirectAccessToProduct.get(userName);
		
		verify(productController).getAll(userName);
		
		assertEquals(productsJson.get(0).getId(), products.get(0).getId());
		assertNotNull(products);
	}
	
	@Test	
	public void getErrorToAccessProductGateway() {
		String userName = "anyEmail";

		doThrow(new RuntimeException()).when(productController).getAll(userName);
		
		assertThrows(ErrorToAccessProductGatewayException.class, () -> {
			stockDirectAccessToProduct.get(userName);
		});
		
		verify(productController).getAll(userName);
		
	}
}
