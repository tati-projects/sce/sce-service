package com.tguisso.sce.stock.gateway.database.mongo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.exception.ErrorToAccessDatabaseGatewayException;

@ExtendWith(MockitoExtension.class)
public class StockMongoGatewayUnitTest {
	
	@InjectMocks
	private StockMongoGateway stockMongoGateway;
	
	@Mock
	private StockRepository stockRepository;
	
	@Test
	public void save() {
		final String id = "anyId";
		final String name = "anyName";
	
		final Stock stock = new Stock(id, name, null, null);
		doReturn(stock).when(stockRepository).save(stock);
		
		String idReturned = stockMongoGateway.save(stock);
		
		verify(stockRepository).save(stock);
		
		assertEquals(id, idReturned);
		
	}
	
	@Test
	public void saveErrorToAccessDatabaseGatewayException() {
		final String id = "anyId";
		final String name = "anyName";
	
		final Stock stock = new Stock(id, name, null, null);
		doThrow(new RuntimeException()).when(stockRepository).save(stock);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			stockMongoGateway.save(stock);
		});
		
		verify(stockRepository).save(stock);
	}
	
	@Test
	public void findByIdAndUserId() {
		final String id = "anyId";
		final String name = "anyName";
		
		final User user = new User("anyUserId", "anyEmail");
		
		Stock stock = new Stock(id, name, null, user);
		Optional<Stock> stockOp = Optional.of(stock);
		doReturn(stockOp).when(stockRepository).findByIdAndUserId(id, user.getId());
		
		Optional<Stock> stockReturned = stockMongoGateway.findByIdAndUserId(id, user.getId());
		
		verify(stockRepository).findByIdAndUserId(id, user.getId());
		
		assertEquals(stock.getId(), stockReturned.get().getId());
		assertEquals(stock.getName(), stockReturned.get().getName());
		assertEquals(stock.getUser(), stockReturned.get().getUser());
		
	}
	
	@Test
	public void findByIdAndUserIdErrorToAccessDatabaseGatewayException() {
		final String id = "anyId";
		final String userId = "anyUserId";
		
		doThrow(new RuntimeException()).when(stockRepository).findByIdAndUserId(id, userId);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			stockMongoGateway.findByIdAndUserId(id, userId);
		});
		
		verify(stockRepository).findByIdAndUserId(id, userId);
				
	}
	
	@Test
	public void findAllByUserId() {
		final String id = "anyId";
		final String name = "anyName";
		final String userId = "anyUserId";
		final String userEmail = "anyEmail";
		
		final User user = new User(userId, userEmail);
		
		List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(id, name, null, user));
		doReturn(stocks).when(stockRepository).findAllByUserId(userId);
		
		List<Stock> stocksReturned = stockMongoGateway.findAllByUserId(userId);
		
		verify(stockRepository).findAllByUserId(userId);
		
		assertEquals(stocks.get(0).getId(), stocksReturned.get(0).getId());
		assertEquals(stocks.get(0).getName(), stocksReturned.get(0).getName());
		assertEquals(stocks.get(0).getUser(), stocksReturned.get(0).getUser());
		
		assertNull(stocksReturned.get(0).getItems());
	}
	
	@Test
	public void findAllByUserIdErrorToAccessDatabaseGatewayException() {
		final String userId = "anyUserId";
		
		doThrow(new RuntimeException()).when(stockRepository).findAllByUserId(userId);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			stockMongoGateway.findAllByUserId(userId);
		});
		
		verify(stockRepository).findAllByUserId(userId);
	}
	
	@Test
	public void deleteById() {
		final String id = "anyId";
		stockMongoGateway.deleteById(id);
		verify(stockRepository).deleteById(id);
	}
	
	@Test
	public void deleteByIdErrorToAccessDatabaseGatewayException() {
		final String id = "anyId";
		
		doThrow(new RuntimeException()).when(stockRepository).deleteById(id);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			stockMongoGateway.deleteById(id);
		});
		
		verify(stockRepository).deleteById(id);
	}
	
	@Test
	public void deleteAll() {
		final String stockId = "anyID";
		final User user = new User("anyID", "anyEmail");
		final List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(stockId, null, null, user));
		
		stockMongoGateway.deleteAll(stocks);
		verify(stockRepository).deleteAll(stocks);
	}
	
	@Test
	public void deleteAllErrorToAccessDatabaseGatewayException() {
		final String stockId = "anyID";
		final User user = new User("anyID", "anyEmail");
		final List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(stockId, null, null, user));
		
		doThrow(new RuntimeException()).when(stockRepository).deleteAll(stocks);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			stockMongoGateway.deleteAll(stocks);
		});

		verify(stockRepository).deleteAll(stocks);
	}
	
	@Test
	public void saveAll() {
		final String stockId = "anyID";
		final String stockName = "anyName";
		final User user = new User("anyID", "anyEmail");
		final Product productA = new Product("Aa");
		final Product productB = new Product("Bb");
		final List<Item> items = new ArrayList<>();
		items.add(new Item(2, productA));
		items.add(new Item(1, productB));
		final List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(stockId, stockName, items, user));
		
		stockMongoGateway.saveAll(stocks);
		
		verify(stockRepository).saveAll(stocks);
	}
	
	@Test
	public void saveAllErrorToAccessDatabaseGatewayException() {
		final String stockId = "anyId";
		final List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(stockId, null, null, null));
		
		doThrow(new RuntimeException()).when(stockRepository).saveAll(stocks);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			stockMongoGateway.saveAll(stocks);
		});
		
		verify(stockRepository).saveAll(stocks);
	}

}
