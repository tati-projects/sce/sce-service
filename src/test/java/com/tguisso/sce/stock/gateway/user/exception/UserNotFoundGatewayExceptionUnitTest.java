package com.tguisso.sce.stock.gateway.user.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class UserNotFoundGatewayExceptionUnitTest {
	
	@Test
	public void userNotFoundGatewayException() {
		SceBaseException exception = new UserNotFoundGatewayException();
		assertEquals("sce.product.user.userNotFoundGateway", exception.getCode());
		assertEquals("User not Found", exception.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
	}

}
