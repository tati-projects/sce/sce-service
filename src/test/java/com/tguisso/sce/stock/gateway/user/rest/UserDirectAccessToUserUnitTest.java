package com.tguisso.sce.stock.gateway.user.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.gateway.user.exception.ErrorToAccessUserServiceGatewayException;
import com.tguisso.sce.stock.gateway.user.exception.UserNotFoundGatewayException;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.user.gateways.http.controllers.UserController;
import com.tguisso.sce.user.gateways.http.controllers.json.UserJson;
import com.tguisso.sce.user.usecases.exception.UserNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class UserDirectAccessToUserUnitTest {
	
	@InjectMocks
	private UserDirectAccessToUser userDirectAccessToUser;
	
	@Mock
	private UserController userController;
	
	@Test
	public void findByEmailSuccess() {
		final String email = "anyEmail";
		final String id = "anyId";
		
		UserJson userJsonToReturn = new UserJson(id, email, null, null);
		doReturn(userJsonToReturn).when(userController).getByEmail(email);
		
		Optional<User> userReturned = userDirectAccessToUser.findByEmail(email);
		
		verify(userController).getByEmail(email);
		
		assertEquals(id, userReturned.get().getId());
		assertEquals(email, userReturned.get().getEmail());
	}
	
	@Test
	public void findByEmailUserNotFoundException() {
		final String email = "anyEmail";
		
		doThrow(new UserNotFoundBusinessException()).when(userController).getByEmail(email);
		
		assertThrows(UserNotFoundGatewayException.class, () -> {
			userDirectAccessToUser.findByEmail(email);
		});
		verify(userController).getByEmail(email);
	}
	
	@Test
	public void findByEmailErrorToAccessUserServiceException() {
		final String email = "anyEmail";
		
		doThrow(new RuntimeException()).when(userController).getByEmail(email);
		
		assertThrows(ErrorToAccessUserServiceGatewayException.class, () -> {
			userDirectAccessToUser.findByEmail(email);
		});
		verify(userController).getByEmail(email);
	}

}
