package com.tguisso.sce.stock.gateway.product.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class ProductNotFoundBusinessExceptionUnitTest {
	
	@Test
	public void productNotFoundBusinessException() {
		SceBaseException exception = new ProductNotFoundBusinessException();
		assertEquals("sce.stock.productNotFound", exception.getCode());
		assertEquals("Product not found.", exception.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
	}

}
