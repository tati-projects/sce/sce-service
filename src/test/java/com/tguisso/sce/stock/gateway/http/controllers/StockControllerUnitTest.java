package com.tguisso.sce.stock.gateway.http.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.http.controllers.json.ItemJson;
import com.tguisso.sce.stock.gateway.http.controllers.json.RequestJson;
import com.tguisso.sce.stock.gateway.http.controllers.json.StockJson;
import com.tguisso.sce.stock.gateway.http.controllers.json.UserJson;
import com.tguisso.sce.stock.usecases.DeleteStockUseCase;
import com.tguisso.sce.stock.usecases.GetStockUseCase;
import com.tguisso.sce.stock.usecases.ItemsOrquestratorUseCase;
import com.tguisso.sce.stock.usecases.SaveStockUseCase;

@ExtendWith(MockitoExtension.class)
public class StockControllerUnitTest {
	
	@InjectMocks
	private StockController stockController;
	
	@Mock
	private SaveStockUseCase saveStockUseCase;
	
	@Mock
	private GetStockUseCase getStockUseCase;
	
	@Mock
	private DeleteStockUseCase deleteStockUseCase;
	
	@Mock
	private ItemsOrquestratorUseCase itemsOrquestratorUseCase;
	
	@Test
	public void create() {
		final String userName = "anyUserEmail";
		final String stockName = "anyName";
		final UserJson userJson = new UserJson("anyUserId", "anyUserEmail");
		final List<ItemJson> itemsJson = new ArrayList<>();
		
		StockJson stockJson = new StockJson(null, stockName, itemsJson, userJson);
		
		String idToReturn = "anyId";
		doReturn(idToReturn).when(saveStockUseCase).save(any(Stock.class), eq(userName));
		
		String idReturned = stockController.create(stockJson, userName);
		
		ArgumentCaptor<Stock> stockAC = ArgumentCaptor.forClass(Stock.class);
		verify(saveStockUseCase).save(stockAC.capture(), eq(userName));
		
		Stock stockSentToUseCase = stockAC.getValue();
		
		assertEquals(idToReturn, idReturned);
		
		assertEquals(stockJson.getId(), stockSentToUseCase.getId());
		assertEquals(stockJson.getName(), stockSentToUseCase.getName());
		assertEquals(stockJson.getItemsJson(), stockSentToUseCase.getItems());
		
		assertNull(stockSentToUseCase.getUser());
		assertNull(stockSentToUseCase.getId());
	}
	
	@Test
	public void update() {
		final String userName = "anyUserEmail";
		final String stockName = "anyName";
		final String stockId = "anyId";
	
		final List<ItemJson> itemsJson = new ArrayList<>();
		
		final UserJson userJson = new UserJson("anyUserId", "anyUserEmail");
		
		StockJson stockJson = new StockJson(stockId, stockName, itemsJson, userJson);
		
		stockController.update(stockJson, userName);
		
		ArgumentCaptor<Stock> stockAC = ArgumentCaptor.forClass(Stock.class);
		verify(saveStockUseCase).update(stockAC.capture(), eq(userName));
		
		Stock stockSentToUseCase = stockAC.getValue();
		
		assertEquals(stockJson.getId(), stockSentToUseCase.getId());
		assertEquals(stockJson.getName(), stockSentToUseCase.getName());
		assertEquals(stockJson.getItemsJson(), stockSentToUseCase.getItems());
	}
	
	@Test
	public void get() {
		final String userId = "anyId";
		final String userName = "anyEmail";
		final String stockName = "anyName";
		final String stockId = "anyId";
		final List<Item> items = new ArrayList<>();
		final User user = new User(userId, userName);
		
		final Stock stock = new Stock(stockId, stockName, items, user);
		doReturn(stock).when(getStockUseCase).get(stockId, userName);
		
		StockJson stockJson = stockController.get(stockId, userName);
		
		verify(getStockUseCase).get(stockId, userName);
		
		assertEquals(stock.getId(), stockJson.getId());
		assertEquals(stock.getName(), stockJson.getName());
		assertEquals(stock.getItems(), stockJson.getItemsJson());
		assertEquals(stock.getUser().getId(), stockJson.getUserJson().getId());
		assertEquals(stock.getUser().getEmail(), stockJson.getUserJson().getEmail());
		
	}
	
	@Test
	public void getAllOneStock() {
		final String userId = "anyId";
		final String userName = "anyEmail";
		final User user = new User(userId, userName);
		final Product productA = new Product("2");
		final Product productB = new Product("5");
		
		final List<Item> items = new ArrayList<>();
		items.add(new Item(2, productA));
		items.add(new Item(7, productB));
		
		final List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock("anyId", "cozinha", items, user));
				
		doReturn(stocks).when(getStockUseCase).getAll(userName);
		
		List<StockJson> stocksJson = stockController.getAll(userName);
		
		verify(getStockUseCase).getAll(userName);
		
		assertEquals(stocks.get(0).getId(), stocksJson.get(0).getId());
		assertEquals(stocks.get(0).getName(), stocksJson.get(0).getName());
		assertEquals(stocks.get(0).getItems().get(0).getQuantity(), stocksJson.get(0).getItemsJson().get(0).getQuantity());
		assertEquals(stocks.get(0).getItems().get(0).getProduct().getId(), stocksJson.get(0).getItemsJson().get(0).getProductId());
		assertEquals(stocks.get(0).getItems().get(1).getQuantity(), stocksJson.get(0).getItemsJson().get(1).getQuantity());
		assertEquals(stocks.get(0).getItems().get(1).getProduct().getId(), stocksJson.get(0).getItemsJson().get(1).getProductId());
		assertEquals(stocks.get(0).getUser().getId(), stocksJson.get(0).getUserJson().getId());
	}
	
	@Test
	public void delete() {
		final String stockId = "anyId";
		final String userName = "anyName";
		
		stockController.delete(stockId, userName);
		
		verify(deleteStockUseCase).delete(stockId, userName);
	}
	
	@Test
	public void addItem() {
		final String userName = "anyName";
		final String stockId = "anyStockId";
		final Integer quantity = 1;
		final String productId = "anyProductId";
		final List<ItemJson> items = new ArrayList<>();
		items.add(new ItemJson(quantity, productId));
		final RequestJson requestJson = new RequestJson(stockId, items);
		
		stockController.updateItem(requestJson, userName);
		
		ArgumentCaptor<Stock> stockAC = ArgumentCaptor.forClass(Stock.class);
		verify(itemsOrquestratorUseCase).changeItems(stockAC.capture(), eq(userName));
		
		Stock stockAddItems = stockAC.getValue();
		
		assertEquals(stockId, stockAddItems.getId());
		assertEquals(quantity, stockAddItems.getItems().get(0).getQuantity());
		assertEquals(productId, stockAddItems.getItems().get(0).getProduct().getId());
		assertNull(stockAddItems.getName());
		assertNull(stockAddItems.getUser());
	}
	
	@Test
	public void deleteByProductId() {
		final String productId = "anyId";
		final String userName = "anyEmail";
		
		stockController.deleteByProductId(productId, userName);

		verify(itemsOrquestratorUseCase).deleteItemByProductId(productId, userName);
	}
	
	@Test
	public void deleteStockByUser() {
		final String userId = "anyId";
		final String userName = "anyEmail";
		
		stockController.deleteByUser(userId, userName);
		
		verify(deleteStockUseCase).deleteByUser(userId, userName);
	}
	
}
