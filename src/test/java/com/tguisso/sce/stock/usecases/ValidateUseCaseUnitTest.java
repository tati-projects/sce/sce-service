package com.tguisso.sce.stock.usecases;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;
import com.tguisso.sce.stock.usecases.exception.StockNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class ValidateUseCaseUnitTest {
	
	@InjectMocks
	private ValidateUseCase validateUseCase;
	
	@Mock
	private StockDBGateway stockDBGateway;
	
	@Test
	public void validateWithSuccess() {
		final String stockId = "anyStockId";
		final String stockName = "anyName"; 
		final String userId = "anyUserId";
		final String userEmail = "anyEmail";
		final User user = new User(userId, userEmail);
		
		Stock stock = new Stock(stockId, stockName, null, user);
		Optional<Stock> stockOp = Optional.of(stock);
		doReturn(stockOp).when(stockDBGateway).findByIdAndUserId(stockId, userId);
		
		validateUseCase.validate(stockId, userId);
		
		verify(stockDBGateway).findByIdAndUserId(stockId, userId);
	}
	
	@Test
	public void validateStockNotFoundBusinessException() {
		final String stockId = "anyStockId";
		final String userId = "anyUserId";
		
		Optional<Stock> stockOp = Optional.empty();
		doReturn(stockOp).when(stockDBGateway).findByIdAndUserId(stockId, userId);
		
		assertThrows(StockNotFoundBusinessException.class, () -> {
			validateUseCase.validate(stockId, userId);
		});
		
		verify(stockDBGateway).findByIdAndUserId(stockId, userId);
	}

}
