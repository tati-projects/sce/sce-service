package com.tguisso.sce.stock.usecases;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;
import com.tguisso.sce.stock.gateway.user.UserGateway;

@ExtendWith(MockitoExtension.class)
public class DeleteStockUseCaseUnitTest {
	
	@InjectMocks
	private DeleteStockUseCase deleteStockUseCase;
	
	@Mock
	private StockDBGateway stockDBGateway;
	
	@Mock
	private UserGateway userGateway;
	
	@Mock
	private ValidateUseCase validateUseCase;
	
	@Mock
	private GetStockUseCase getStockUseCase;
	
	@Test
	public void delete() {
		final String stockId = "anyStockId";
		final String userId = "anyUserId";
		final String userName = "anyName";
		
		final User user = new User(userId, userName);
		final Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userName);
		
		deleteStockUseCase.delete(stockId, userName);
		
		verify(userGateway).findByEmail(userName);
		verify(validateUseCase).validate(stockId, user.getId());
		
	}
	
	@Test
	public void deleteStockByUser() {
		final String stockIdA = "Aa";
		final String stockIdB = "Bb";
		final User user = new User("anyId", "anyEmail");
		
		List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(stockIdA, null, null, user));
		stocks.add(new Stock(stockIdB, null, null, user));
		
		doReturn(stocks).when(getStockUseCase).getAll(user.getEmail());
		
		deleteStockUseCase.deleteByUser(user.getId(), user.getEmail());
		
		verify(getStockUseCase).getAll(user.getEmail());
		verify(stockDBGateway).deleteAll(stocks);
	}
	
}
