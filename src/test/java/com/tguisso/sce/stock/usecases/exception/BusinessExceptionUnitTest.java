package com.tguisso.sce.stock.usecases.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class BusinessExceptionUnitTest {
	
	@Test
	public void stockNotFoundBusinessException() {
		SceBaseException exception = new StockNotFoundBusinessException();
		assertEquals("sce.stock.stockNotFound", exception.getCode());
		assertEquals("Stock Not Found", exception.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
	}
	
	@Test
	public void userUnauthorizedBusinessException() {
		SceBaseException exception = new UserUnauthorizedBusinessException();
		assertEquals("sce.stock.user.userUnauthorized", exception.getCode());
		assertEquals("User unauthorized", exception.getMessage());
		assertEquals(HttpStatus.UNAUTHORIZED, exception.getHttpStatus());
	}
	
	@Test
	public void invalidQuantityBusinessException() {
		SceBaseException exception = new InvalidQuantityBusinessException();
		assertEquals("sce.stock.invalidQuantity", exception.getCode());
		assertEquals("Invalid Quantity", exception.getMessage());
		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
	}
}
