package com.tguisso.sce.stock.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;
import com.tguisso.sce.stock.gateway.user.UserGateway;

@ExtendWith(MockitoExtension.class)
public class GetStockUseCaseUnitTest {
	
	@InjectMocks
	private GetStockUseCase getStockUseCase;
	
	@Mock
	private StockDBGateway stockDBGateway;
	
	@Mock
	private ValidateUseCase validateUseCase;
	
	@Mock
	private UserGateway userGateway;
	
	@Test
	public void get() {
		final String stockId = "anyId";
		final String stockName = "anyName";
		final String userId = "anyUserId";
		final String userName = "anyName";
		final Product productA = new Product("2");
		
		final List<Item> items = new ArrayList<>();
		items.add(new Item(2, productA));
		
		final User user = new User(userId, userName);
		final Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userName);
		
		final Stock stockToReturn = new Stock(stockId, stockName, items, user);
		final Optional<Stock> stockOp = Optional.of(stockToReturn);
		doReturn(stockOp).when(stockDBGateway).findByIdAndUserId(stockId, userId);
		
		Stock stockReturned = getStockUseCase.get(stockId, userName);
		
		verify(userGateway).findByEmail(userName);
		verify(stockDBGateway).findByIdAndUserId(stockId, userId);
		
		assertEquals(stockToReturn.getId(), stockReturned.getId());
		assertEquals(stockToReturn.getName(), stockReturned.getName());
		assertEquals(stockToReturn.getUser().getId(), stockReturned.getUser().getId());
		assertEquals(stockToReturn.getUser().getEmail(), stockReturned.getUser().getEmail());
		assertEquals(stockToReturn.getItems().get(0).getQuantity(), stockReturned.getItems().get(0).getQuantity());
		assertEquals(stockToReturn.getItems().get(0).getProduct(), stockReturned.getItems().get(0).getProduct());
		
	}
	
	@Test
	public void getAll() {
		final String stockId = "anyId";
		final String stockName = "anyName";
		final String userId = "anyUserId";
		final String userName = "anyName";
		final Product productA = new Product("2");
		
		final List<Item> items = new ArrayList<>();
		items.add(new Item(2, productA));
		
		final User user = new User(userId, userName);
		final Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userName);
		
		final List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(stockId, stockName, items, user));
		
		doReturn(stocks).when(stockDBGateway).findAllByUserId(user.getId());
		
		List<Stock> stocksReturned = getStockUseCase.getAll(userName);
		
		verify(userGateway).findByEmail(userName);
		verify(stockDBGateway).findAllByUserId(user.getId());
		
		assertEquals(stocks.get(0).getId(), stocksReturned.get(0).getId());
		assertEquals(stocks.get(0).getName(), stocksReturned.get(0).getName());
		assertEquals(stocks.get(0).getUser().getId(), stocksReturned.get(0).getUser().getId());
		assertEquals(stocks.get(0).getUser().getEmail(), stocksReturned.get(0).getUser().getEmail());
		assertEquals(stocks.get(0).getItems().get(0).getQuantity(), stocksReturned.get(0).getItems().get(0).getQuantity());
		assertEquals(stocks.get(0).getItems().get(0).getProduct(), stocksReturned.get(0).getItems().get(0).getProduct());
		
	}

}
