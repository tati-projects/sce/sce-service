package com.tguisso.sce.stock.usecases;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.user.UserGateway;
import com.tguisso.sce.stock.usecases.exception.UserUnauthorizedBusinessException;

@ExtendWith(MockitoExtension.class)
public class StockBaseUseCaseUnitTest {
	
	@InjectMocks
	private StockBaseUseCase stockBaseUseCase =  new SaveStockUseCase();
	
	@Mock
	private UserGateway userGateway;
	
	@Test
	public void getUserByUserName() {
		final String userEmail = "anyEmail";
		final String userId = "anyId";
		
		final User userToReturn = new User(userId, userEmail);
		Optional<User> userOp = Optional.of(userToReturn);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		User userReturned =  stockBaseUseCase.getUserByUserName(userEmail);
		
		verify(userGateway).findByEmail(userEmail);
		
		assertEquals(userToReturn.getId(), userReturned.getId());
		assertEquals(userToReturn.getEmail(), userReturned.getEmail());
	}
	
	@Test
	public void getUserByNameUserUnauthorizedBusinessException() {
		final String userEmail = "anyEmail";
		
		Optional<User> userOp = Optional.empty();
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			stockBaseUseCase.getUserByUserName(userEmail);
		});
		
		verify(userGateway).findByEmail(userEmail);
	}
}
