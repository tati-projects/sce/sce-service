package com.tguisso.sce.stock.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;

@ExtendWith(MockitoExtension.class)
public class ItemsOrquestratorUseCaseUnitTest {
	
	@InjectMocks
	private ItemsOrquestratorUseCase itemsOrquestratorUseCase;
	
	@Mock
	private StockDBGateway stockDBGateway;
	
	@Mock
	private GetStockUseCase getStockUseCase;
	
	@Mock
	private ValidateProductUseCase validateProductUseCase;
	
	@Mock
	private ValidateMinimumQuantityUseCase validateMinimumQuantityUseCase;
	
	@Test
	public void add() {
		final String userName = "anyName";
		final String stockId = "anyStockID";
		final String name = "anyStockName";
		final Product productA = new Product("productA");
		final Product productB = new Product("productB");
		
		final List<Item> updatedItems = new ArrayList<>();
		updatedItems.add(new Item(2, productA));
		updatedItems.add(new Item(1, productB));
		
		final Stock stockReceived = new Stock(stockId, null, updatedItems, null);
		
		final List<Item> items = new ArrayList<>();
		final Stock stockToAddItem = new Stock(stockId, name, items, null);
		doReturn(stockToAddItem).when(getStockUseCase).get(stockId, userName);
		
		itemsOrquestratorUseCase.changeItems(stockReceived, userName);
		
		verify(getStockUseCase).get(stockId, userName);
		verify(validateProductUseCase).validate(stockReceived.getItems(), userName);
		verify(validateMinimumQuantityUseCase).validateMinimumQuantity(stockReceived.getItems());
		verify(stockDBGateway).save(stockToAddItem);
		
		assertEquals(stockId, stockToAddItem.getId());
		assertEquals(2, stockToAddItem.getItems().size());
		
		assertEquals(2, stockToAddItem.getItems().get(0).getQuantity());
		assertEquals(productA, stockToAddItem.getItems().get(0).getProduct());
		assertEquals(stockReceived.getItems().get(0), stockToAddItem.getItems().get(0));
		
		assertEquals(1, stockToAddItem.getItems().get(1).getQuantity());
		assertEquals(productB, stockToAddItem.getItems().get(1).getProduct());
		assertEquals(stockReceived.getItems().get(1), stockToAddItem.getItems().get(1));
		
	}
	
	@Test
	public void remove() {
		final String userName = "anyName";
		final String stockId = "anyStockID";
		final String name = "anyStockName";
		final Product productA = new Product("idA");
		final Product productB = new Product("idB");
		final Product productC = new Product("idC");
		final Product productD = new Product("idD");
		final Product productE = new Product("idE");
		final Product productF = new Product("idF");
		
		final List<Item> updatedItems = new ArrayList<>();
		updatedItems.add(new Item(0, productA));
		updatedItems.add(new Item(2, productB));
		updatedItems.add(new Item(0, productC));
		updatedItems.add(new Item(1, productD));
		updatedItems.add(new Item(0, productE));
		updatedItems.add(new Item(1, productF));
		
		final Stock stockReceived = new Stock(stockId, null, updatedItems, null);
		
		final List<Item> items = new ArrayList<>();
		items.add(new Item(3, productA));
		items.add(new Item(2, productB));
		items.add(new Item(1, productC));
		items.add(new Item(1, productD));
		items.add(new Item(2, productE));
		items.add(new Item(3, productF));
		
		final Stock stockToAddItem = new Stock(stockId, name, items, null);
		doReturn(stockToAddItem).when(getStockUseCase).get(stockId, userName);
		
		itemsOrquestratorUseCase.changeItems(stockReceived, userName);
		
		verify(getStockUseCase).get(stockId, userName);
		verify(validateProductUseCase).validate(stockReceived.getItems(), userName);
		verify(validateMinimumQuantityUseCase).validateMinimumQuantity(stockReceived.getItems());
		verify(stockDBGateway).save(stockToAddItem);
		
		assertEquals(stockId, stockToAddItem.getId());
		
		assertEquals(3 ,stockToAddItem.getItems().size());
		
	 	assertEquals(productB, stockToAddItem.getItems().get(0).getProduct());
		assertEquals(2, stockToAddItem.getItems().get(0).getQuantity());
		assertEquals(stockReceived.getItems().get(0), stockToAddItem.getItems().get(0));
		
		assertEquals(productD, stockToAddItem.getItems().get(1).getProduct());
		assertEquals(1, stockToAddItem.getItems().get(1).getQuantity());
		assertEquals(stockReceived.getItems().get(1), stockToAddItem.getItems().get(1));
		
		assertEquals(productF, stockToAddItem.getItems().get(2).getProduct());
		assertEquals(1, stockToAddItem.getItems().get(2).getQuantity());
		assertEquals(stockReceived.getItems().get(2), stockToAddItem.getItems().get(2));
	}
	
	@Test
	public void deleteByProductId() {
		String productId = "Aa";
		String userEmail = "anyEmail";
		String userName = "anyName";
		User user = new User(userName, userEmail);
		
		Product productA = new Product("Aa");
		Product productB = new Product("Bb");
		Product productC = new Product("Cc");
		
		List<Item> items = new ArrayList<>();
		items.add(new Item(2, productA));
		items.add(new Item(1, productB));
		items.add(new Item(2, productC));
		
		List<Stock> stocks = new ArrayList<>();
		stocks.add(new Stock(null, null, items, user));
		doReturn(stocks).when(getStockUseCase).getAll(userEmail);
		
		itemsOrquestratorUseCase.deleteItemByProductId(productId, userEmail);
		
		verify(getStockUseCase).getAll(userEmail);
		verify(stockDBGateway).saveAll(stocks);
		
		assertEquals(2, stocks.get(0).getItems().size());
		assertEquals(productB.getId(), stocks.get(0).getItems().get(0).getProduct().getId());
		assertEquals(productC.getId(), stocks.get(0).getItems().get(1).getProduct().getId());
		assertEquals(userEmail, user.getEmail());
		
	}

}
