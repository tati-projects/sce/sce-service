package com.tguisso.sce.stock.usecases;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.usecases.exception.InvalidQuantityBusinessException;

public class ValidateMinimumQuantityUseCaseUnitTest {
	
	private ValidateMinimumQuantityUseCase validateMinimumQuantityUseCase = new ValidateMinimumQuantityUseCase();
	
	@Test
	public void validateMinimimQuantity() {
		Product productA = new Product("Aa");
		Product productB = new Product("Bb");
		Product productC = new Product("Cc");
		
		List<Item> items = new ArrayList<>();
		items.add(new Item(0, productA));
		items.add(new Item(1, productB));
		items.add(new Item(2, productC));
		
		validateMinimumQuantityUseCase.validateMinimumQuantity(items);
	}
	
	@Test
	public void validateInvalidQuantityBusinessException() {
		Product productA = new Product("Aa");
		Product productB = new Product("Bb");
		Product productC = new Product("Cc");
		Product productD = new Product("Dd");
		
		List<Item> items = new ArrayList<>();
		items.add(new Item(0, productA));
		items.add(new Item(1, productC));
		items.add(new Item(-1, productB));
		items.add(new Item(2, productD));
		
		assertThrows(InvalidQuantityBusinessException.class, () -> {
			validateMinimumQuantityUseCase.validateMinimumQuantity(items);
		});
	}

}
