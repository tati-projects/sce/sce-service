package com.tguisso.sce.stock.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;
import com.tguisso.sce.stock.gateway.user.UserGateway;

@ExtendWith(MockitoExtension.class)
public class SaveStockUseCaseUnitTest {
	
	@InjectMocks
	private SaveStockUseCase saveStockUseCase;
	
	@Mock
	private UserGateway userGateway;
	
	@Mock
	private StockDBGateway stockDBGateway;
	
	@Mock
	private GetStockUseCase getStockUseCase;
	
	@Test
	public void save() {
		final String stockName = "anyName";
		final String userName = "anyUserName";
		final String userId = "anyUserId";
		final User user = new User(userId, userName);
		
		final Stock stock = new Stock(null, stockName, null, user);
		
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userName);
		
		String idToReturn = "anyId";
		doReturn(idToReturn).when(stockDBGateway).save(any(Stock.class));
		
		String idReturned = saveStockUseCase.save(stock, userName);

		verify(userGateway).findByEmail(userName);
		
		ArgumentCaptor<Stock> stockAC = ArgumentCaptor.forClass(Stock.class);
		verify(stockDBGateway).save(stockAC.capture());
		
		Stock stockSaved = stockAC.getValue();
		
		assertEquals(idToReturn, idReturned);
		assertEquals(stock.getName(), stockSaved.getName());
		assertEquals(user, stockSaved.getUser());
		assertNull(stockSaved.getId());
		assertNull(stockSaved.getItems());
	}
	
	@Test
	public void updateNew() {
		final String newStockName = "otherName";
		final String stockName = "anyName";
		final String stockId = "anyId";
		final String userName = "anyUserName";
		final String userId = "anyUserId";
		final Product product = new Product("ab45");
		final List<Item> items = new ArrayList<>();
		items.add(new Item(2, product));
		
		final Stock stockToUpdate = new Stock(stockId, newStockName, null, null);
		
		final User user = new User(userId, userName);
		final Stock stockExistent = new Stock(stockId, stockName, items, user);
		doReturn(stockExistent).when(getStockUseCase).get(stockId, userName);
		
		saveStockUseCase.update(stockToUpdate, userName);
		
		verify(getStockUseCase).get(stockId, userName);
		
		ArgumentCaptor<Stock> stockAC = ArgumentCaptor.forClass(Stock.class);
		verify(stockDBGateway).save(stockAC.capture());
		
		Stock stockToSave = stockAC.getValue();
		
		assertEquals(stockToSave.getId(), stockToUpdate.getId());
		assertEquals(stockToSave.getId(), stockExistent.getId());
		assertEquals(stockToSave.getName(), stockToUpdate.getName());
		assertEquals(stockToSave.getItems(), stockExistent.getItems());
		assertEquals(stockToSave.getUser().getId(), stockExistent.getUser().getId());
		assertEquals(stockToSave.getUser().getEmail(), stockExistent.getUser().getEmail());
		
		assertNotEquals(stockToSave.getName(), stockExistent.getName());

		assertNull(stockToUpdate.getItems());
		assertNull(stockToUpdate.getUser());
	}

}
