package com.tguisso.sce.stock.usecases;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.gateway.product.ProductGateway;
import com.tguisso.sce.stock.gateway.product.exception.ProductNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class ValidateProductUseCaseUnitTest {
	
	@InjectMocks
	private ValidateProductUseCase validateProductUseCase;
	
	@Mock
	private ProductGateway productGateway;
	
	@Test
	public void validateProductSuccess() {
		final String userName = "anyName";
		final Product product1 = new Product("Aa");
		final Product product2 = new Product("Bb");
		final Product product3 = new Product("Cc");
		
		final List<Item> items = new ArrayList<>();
		items.add(new Item(1, product3));
		items.add(new Item(1, product1));
		items.add(new Item(1, product2));	

		final List<Product> products = new ArrayList<>();
		products.add(new Product("Aa"));
		products.add(new Product("Bb"));
		products.add(new Product("Cc"));
		doReturn(products).when(productGateway).get(userName);
		
		validateProductUseCase.validate(items, userName);
		
		verify(productGateway).get(userName);
		
	}
	
	@Test
	public void validateProductNotFoundBusinessException() {
		final String userName = "anyName";
		final Product product1 = new Product("Aa");
		final Product product2 = new Product("Bb");
		final Product product3 = new Product("Cc");
		
		final List<Item> items = new ArrayList<>();
		items.add(new Item(1, product1));
		items.add(new Item(1, product2));	
		items.add(new Item(1, product3));

		final List<Product> products = new ArrayList<>();
		products.add(new Product("Aa"));
		products.add(new Product("Bb"));
		products.add(new Product("Uu"));
		doReturn(products).when(productGateway).get(userName);
		
		assertThrows(ProductNotFoundBusinessException.class, () -> {
			validateProductUseCase.validate(items, userName);
		});
		
		verify(productGateway).get(userName);
		
	}

}
