package com.tguisso.sce.product.gateway.http.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.http.controllers.json.ProductJson;
import com.tguisso.sce.product.usecases.DeleteProductUseCase;
import com.tguisso.sce.product.usecases.GetProductUseCase;
import com.tguisso.sce.product.usecases.SaveProductUseCase;

@ExtendWith(MockitoExtension.class)
public class ProductControllerUnitTest {
	
	@InjectMocks
	private ProductController productController;
	
	@Mock
	private SaveProductUseCase saveProductUseCase;
	
	@Mock
	private GetProductUseCase getProductUseCase;
	
	@Mock
	private DeleteProductUseCase deleteProductUseCase;
	
	@Test
	public void create() {
		final String idProductToReturn = "anyIdProduct";
		final String name = "anyName";
		final String description = "anyDescription";
		final String barCode = "anyBarCode";
		final String userName = "anyUserName";
		final User user = new User("anyUserId", "anyUserEmail");
		ProductJson productJson = new ProductJson(null, name, description, barCode, user); 
		
		doReturn(idProductToReturn).when(saveProductUseCase).save(any(Product.class), eq(userName));
		
		String idProductToReturned = productController.create(productJson, userName);
		
		ArgumentCaptor<Product> productAC = ArgumentCaptor.forClass(Product.class);
		verify(saveProductUseCase).save(productAC.capture(), eq(userName));
		
		assertEquals(idProductToReturn, idProductToReturned);
		
		Product productSentToUseCase = productAC.getValue();
		
		assertNull(productSentToUseCase.getId());
		assertEquals(productJson.getName(), productSentToUseCase.getName());
		assertEquals(productJson.getDescription(), productSentToUseCase.getDescription());
		assertEquals(productJson.getBarCode(), productSentToUseCase.getBarCode());
		assertEquals(productJson.getUser().getId(), productSentToUseCase.getUser().getId());
	}
	
	@Test
	public void update() {
		final String id = "anyIdProduct";
		final String name = "otherName";
		final String description = "otherDescription";
		final String barCode = "anyBarCode";
		final String userName = "anyUserName";
		final User user = new User("anyUserId", "anyUserEmail");
		ProductJson productJson = new ProductJson(id, name, description, barCode, user); 
		
		productController.update(productJson, userName);
		
		ArgumentCaptor<Product> productAC = ArgumentCaptor.forClass(Product.class);
		verify(saveProductUseCase).update(productAC.capture(), eq(userName));
		
		Product productSentToUseCase = productAC.getValue();
		
		assertEquals(productJson.getId(), productSentToUseCase.getId());
		assertEquals(productJson.getName(), productSentToUseCase.getName());
		assertEquals(productJson.getDescription(), productSentToUseCase.getDescription());
		assertEquals(productJson.getBarCode(), productSentToUseCase.getBarCode());
		assertEquals(productJson.getUser().getId(), productSentToUseCase.getUser().getId());
	}
	
	@Test
	public void get() {
		final String idProduct = "anyIdProduct";
		final String userName = "anyUserName";
		final String id = "anyIdProduct";
		final String name = "anyName";
		final String description = "anyDescription";
		final String barCode = "anyBarCode";
		final User user = new User("anyUserId", "anyUserEmail");

		final Product product = new Product(id, name, description, barCode, user);
		
		doReturn(product).when(getProductUseCase).get(idProduct, userName);
		
		ProductJson productJsonToReturn = productController.get(idProduct, userName);
		verify(getProductUseCase).get(idProduct, userName);
		
		assertEquals(product.getId(), productJsonToReturn.getId());
		assertEquals(product.getName(), productJsonToReturn.getName());
		assertEquals(product.getDescription(), productJsonToReturn.getDescription());
		assertEquals(product.getBarCode(), productJsonToReturn.getBarCode());
		assertEquals(product.getUser().getId(), productJsonToReturn.getUser().getId());
	}
	
	@Test
	public void getAll() {
		final String userName = "anyUserName";
		final String productId = "anyIdProduct";
		final String productName = "anyProductName";
		final String productDescription = "anyProductDescription";
		final String productBarCode = "anyProductBarCode";
		final User user = new User("anyUserId", "anyUserEmail");
		
		List<Product> productsToReturn = new ArrayList<>();
		productsToReturn.add(new Product(productId, productName, productDescription, productBarCode, user));
		doReturn(productsToReturn).when(getProductUseCase).getAll(userName);
		
		List<ProductJson> productsJsonReturned = productController.getAll(userName);
		verify(getProductUseCase).getAll(userName);
		
		assertEquals(productId, productsJsonReturned.get(0).getId());
		assertEquals(productName, productsJsonReturned.get(0).getName());
		assertEquals(productDescription, productsJsonReturned.get(0).getDescription());
		assertEquals(productBarCode, productsJsonReturned.get(0).getBarCode());
		assertEquals(user.getId(), productsJsonReturned.get(0).getUser().getId());
	}
	
	@Test
	public void delete() {
		String idProduct = "anyIdProduct";
		String userName = "anyUserName";
		productController.delete(idProduct, userName);
		verify(deleteProductUseCase).delete(idProduct, userName);
	}
	
	@Test
	public void deleteByUser() {
		final String userId = "anyId";
		final String userName = "anyName";
		
		productController.deleteByUser(userId, userName);
		
		verify(deleteProductUseCase).deleteByUser(userId, userName);
	}

}
