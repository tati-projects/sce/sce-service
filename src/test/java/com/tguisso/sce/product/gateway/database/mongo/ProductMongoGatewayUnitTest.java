package com.tguisso.sce.product.gateway.database.mongo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.gateway.database.exception.ErrorToAccessDatabaseGatewayException;

@ExtendWith(MockitoExtension.class)
public class ProductMongoGatewayUnitTest {
	
	@InjectMocks
	private ProductDBGateway productMongoGateway = new ProductMongoGateway();
	
	@Mock
	private ProductRepository productRepository;
	
	@Test
	public void saveProductWithSuccess() {
		final String productId = "anyIdProduct";
		final String productName = "anyProductName";
		final String productDescription = "anyProductDescription";
		final String productBarCode = "anyProductBarCode";
		final User user = new User("anyUserId", "anyUserEmail");
		
		Product product = new Product(
				productId, 
				productName, 
				productDescription, 
				productBarCode, 
				user);
		doReturn(product).when(productRepository).save(product);
		
		String idReturned = productMongoGateway.save(product);
		
		verify(productRepository).save(product);
		
		assertEquals(productId, idReturned);
		
	}
	
	@Test
	public void saveProductErrorToAccessDatabaseGatewayException() {
		final String productId = null;
		final String productName = "anyProductName";
		final String productDescription = "anyProductDescription";
		final String productBarCode = "anyProductBarCode";
		final User user = new User("anyUserId", "anyUserEmail");
		
		Product product = new Product(
				productId, 
				productName, 
				productDescription, 
				productBarCode, 
				user);
		
		doThrow(new RuntimeException()).when(productRepository).save(product);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			(productMongoGateway).save(product);
		});
		
		verify(productRepository).save(product);
	}
	
	@Test
	public void deleteByIdSuccess() {
		final String id = "anyIdProduct";
		
		productMongoGateway.deleteById(id);
		
		verify(productRepository).deleteById(id);
	}
	
	@Test
	public void deleteByIdErrorToAccessDatabaseException() {
		final String id = "anyIdProduct";
		
		doThrow(new RuntimeException()).when(productRepository).deleteById(id);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			productMongoGateway.deleteById(id);
		});
		
		verify(productRepository).deleteById(id);
	}
	
	@Test
	public void deleteProductByUserId() {
		List<Product> products = new ArrayList<>();
		products.add(new Product(null, "aa", null, null, null));
		products.add(new Product(null, "bb", null, null, null));
		
		productMongoGateway.deleteAll(products);
		verify(productRepository).deleteAll(products);
	}
	
	@Test
	public void deleteProductByUserIdErrorToAccessDatabaseGatewayException() {
		List<Product> products = new ArrayList<>();
		doThrow(new RuntimeException()).when(productRepository).deleteAll(products);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			productMongoGateway.deleteAll(products);
		});

		verify(productRepository).deleteAll(products);
	}
	
	@Test
	public void findByIdAndUserIdWithSuccess() {
		final String id = "anyIdProduct";
		final String productName = "anyProductName";
		final String productDescription = "anyProductDescription";
		final String productBarCode = "anyProductBarCode";
		final User user = new User("anyUserId", "anyUserEmail");
		
		Product productToReturn = new Product(id, productName, productDescription, productBarCode, user);
		
		Optional<Product> productOp = Optional.of(productToReturn);
		doReturn(productOp).when(productRepository).findByIdAndUserId(id, user.getId());
		
		Optional<Product> productReturned = productMongoGateway.findByIdAndUserId(id, user.getId());
		
		verify(productRepository).findByIdAndUserId(id, user.getId());
		
		assertEquals(id, productReturned.get().getId());
		assertEquals(productName, productReturned.get().getName());
		assertEquals(productDescription, productReturned.get().getDescription());
		assertEquals(productBarCode, productReturned.get().getBarCode());
		assertEquals(user.getId(), productReturned.get().getUser().getId());
		
	}
	
	@Test
	public void findByIdAndUserIdErrorToAccessDatabaseException() {
		final String id = "anyIdProduct";
		final String userId = "anyUserId";
		
		doThrow(new RuntimeException()).when(productRepository).findByIdAndUserId(id, userId);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			productMongoGateway.findByIdAndUserId(id, userId);
		}); 
		
		verify(productRepository).findByIdAndUserId(id, userId);
	}
	
	@Test
	public void findAllByUserId() {
		final String id = "anyIdProduct";
		final String productName = "anyProductName";
		final String productDescription = "anyProductDescription";
		final String productBarCode = "anyProductBarCode";
		final User user = new User("anyUserId", "anyUserEmail");
		
		List<Product> products = new ArrayList<>();
		products.add(new Product(id, productName, productDescription, productBarCode, user));
		
		doReturn(products).when(productRepository).findAllByUserId(user.getId());
		
		List<Product> productsReturned = productMongoGateway.findAllByUserId(user.getId());
		
		verify(productRepository).findAllByUserId(user.getId());
		
		assertEquals(id, productsReturned.get(0).getId());
		assertEquals(productName, productsReturned.get(0).getName());
		assertEquals(productDescription, productsReturned.get(0).getDescription());
		assertEquals(productBarCode, productsReturned.get(0).getBarCode());
		assertEquals(user.getId(), productsReturned.get(0).getUser().getId());
		assertNotNull(productsReturned);
	}
	
	@Test
	public void findAllByUserIdErrorToAccessDatabaseException() {
		final String userId = "anyUserId";
		
		doThrow(new RuntimeException()).when(productRepository).findAllByUserId(userId);
		
		assertThrows(ErrorToAccessDatabaseGatewayException.class, () -> {
			productMongoGateway.findAllByUserId(userId);
		}); 
		
		verify(productRepository).findAllByUserId(userId);
		
	}

}
