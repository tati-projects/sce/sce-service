package com.tguisso.sce.product.gateway.user.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class UserServiceGatewayExceptionUnitTest {
	
	@Test
	public void errorToAccessUserServiceGatewayException() {
		SceBaseException exception = new ErrorToAccessUserServiceGatewayException();
		assertEquals("sce.product.user.errorToAccessUserService", exception.getCode());
		assertEquals("Error to access user service", exception.getMessage());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
	}
	
	@Test
	public void userNotFoundGatewayException() {
		SceBaseException exception = new UserNotFoundGatewayException();
		assertEquals("sce.product.user.userNotFoundGateway", exception.getCode());
		assertEquals("User not Found", exception.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
	}

}
