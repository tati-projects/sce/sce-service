package com.tguisso.sce.product.gateway.stock.rest;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doThrow;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.gateway.stock.exception.ErrorToAccessStockServiceGatewayException;
import com.tguisso.sce.stock.gateway.http.controllers.StockController;

@ExtendWith(MockitoExtension.class)
public class StockDirectAccessToStockUnitTest {
	
	@InjectMocks
	private StockDirectAccessToStock stockDirectAccessToStock;
	
	@Mock
	private StockController stockController;
	
	@Test
	public void deleteByProductIdSuccess() {
		final String productId = "anyId";
		final String userName = "anyEmail";
		
		stockDirectAccessToStock.deleteByProductId(productId, userName);
		
		verify(stockController).deleteByProductId(productId, userName);
	}
	
	@Test
	public void deleteByProductIdErrorToAccessUserServiceGatewayException() {
		final String productId = "anyId";
		final String userName = "anyEmail";
		
		doThrow(new RuntimeException()).when(stockController).deleteByProductId(productId, userName);
		
		assertThrows(ErrorToAccessStockServiceGatewayException.class, () -> {
			stockDirectAccessToStock.deleteByProductId(productId, userName);
			
		});
		
	}

}
