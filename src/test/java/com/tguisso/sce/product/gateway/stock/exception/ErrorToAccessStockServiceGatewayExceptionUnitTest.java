package com.tguisso.sce.product.gateway.stock.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class ErrorToAccessStockServiceGatewayExceptionUnitTest {
	
	@Test
	public void errorToAccessStockServiceGatewayException() {
		SceBaseException exception = new ErrorToAccessStockServiceGatewayException(); 
		assertEquals("sce.product.stock.errorToAccessStockService", exception.getCode());
		assertEquals("Error to access stock service", exception.getMessage());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
	}
}
