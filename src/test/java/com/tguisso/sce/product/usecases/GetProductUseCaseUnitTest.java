package com.tguisso.sce.product.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.gateway.user.UserGateway;

@ExtendWith(MockitoExtension.class)
public class GetProductUseCaseUnitTest {
	
	@InjectMocks
	private GetProductUseCase getProductUseCase;
	
	@Mock
	private UserGateway userGateway;
	
	@Mock
	private ValidateUseCase validateUseCase;
	
	@Mock
	private ProductDBGateway productDBGateway;
	
	@Test
	public void get() {
		final String productId = "anyIdProduct";
		final String userEmail = "anyEmail";
		final String productName = "anyName";
		final String description = "anyDescription";
		final String barCode = "anyBarCode";
		final String userId = "anyUserId";
		
		final User user = new User(userId, userEmail);
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		final Product productToReturn = new Product(productId, productName, description, barCode, user);
		Optional<Product> productOp = Optional.of(productToReturn);
		doReturn(productOp).when(productDBGateway).findByIdAndUserId(productId, userId);
		
		Product productReturned = getProductUseCase.get(productId, userEmail);
		
		verify(userGateway).findByEmail(userEmail);
		verify(validateUseCase).validate(productId, userId);
		verify(productDBGateway).findByIdAndUserId(productId, userId);
		
		assertEquals(productToReturn.getId(), productReturned.getId());
		assertEquals(productToReturn.getName(), productReturned.getName());
		assertEquals(productToReturn.getDescription(), productReturned.getDescription());
		assertEquals(productToReturn.getBarCode(), productReturned.getBarCode());
		assertEquals(productToReturn.getUser().getId(), productReturned.getUser().getId());
		
	}
	
	@Test
	public void getAllProduct() {
		final String userId = "anyUserId";
		final String userEmail = "anyUserEmail";
		final String productId = "anyIdProduct";
		final String productName = "anyName";
		final String description = "anyDescription";
		final String barCode = "anyBarCode";
		
		final User user = new User(userId, userEmail);
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		final List<Product> productsToReturn = new ArrayList<>();
		productsToReturn.add(new Product(productId, productName, description, barCode, user));
		doReturn(productsToReturn).when(productDBGateway).findAllByUserId(userId);
		
		List<Product> productsReturned = getProductUseCase.getAll(userEmail); 
		
		verify(userGateway).findByEmail(userEmail);
		verify(productDBGateway).findAllByUserId(userId);
		
		assertNotNull(productsReturned);
		
		assertEquals(productsToReturn.get(0).getId(), productsReturned.get(0).getId());
		assertEquals(productsToReturn.get(0).getName(), productsReturned.get(0).getName());
		assertEquals(productsToReturn.get(0).getDescription(), productsReturned.get(0).getDescription());
		assertEquals(productsToReturn.get(0).getBarCode(), productsReturned.get(0).getBarCode());
		assertEquals(productsToReturn.get(0).getUser().getId(), productsReturned.get(0).getUser().getId());
	}
	
}
