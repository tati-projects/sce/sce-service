package com.tguisso.sce.product.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.gateway.user.UserGateway;
import com.tguisso.sce.product.usecases.exception.UserUnauthorizedBusinessException;


@ExtendWith(MockitoExtension.class)
public class SaveProductUseCaseUnitTest {
	
	@InjectMocks
	private SaveProductUseCase saveProductUseCase;
	
	@Mock
	private UserGateway userGateway;
	
	@Mock
	private ProductDBGateway productDBGateway;
	
	@Mock
	private ValidateUseCase validateUseCase;
	
	@Test
	public void saveWithSuccess() {
		final String productIdToReturn = "anyIdProduct";
		final String userId = "anyUserId";
		final String userEmail = "anyUserEmail";
		final String productName = "anyName";
		final String productDescription = "anyDescription";
		final String productBarCode = "anyBarCode";
		
		final Product product = new Product(null, productName, productDescription, productBarCode, null);
		
		final User userToReturn = new User(userId, userEmail);
		Optional<User> userOp = Optional.of(userToReturn);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		doReturn(productIdToReturn).when(productDBGateway).save(any(Product.class));
		
		String savedProductId = saveProductUseCase.save(product, userEmail);
		
		verify(userGateway).findByEmail(userEmail);

		ArgumentCaptor<Product> productAC = ArgumentCaptor.forClass(Product.class);
		verify(productDBGateway).save(productAC.capture());
		
		Product productSaved = productAC.getValue();
		
		assertEquals(productIdToReturn, savedProductId);
		assertEquals(product.getName(), productSaved.getName());
		assertEquals(product.getDescription(), productSaved.getDescription());
		assertEquals(product.getBarCode(), productSaved.getBarCode());
		assertEquals(userToReturn.getId(), productSaved.getUser().getId());
	}
	
	@Test
	public void unauthorizedCreateProductUserNotFound() {
		final String userEmail = "anyUserEmail";
		final String productName = "anyName";
		final String productDescription = "anyDescription";
		final String productBarCode = "anyBarCode";
		
		final Product product = new Product(null, productName, productDescription, productBarCode, null);
		
		final Optional<User> userEmpty = Optional.empty();
		doReturn(userEmpty).when(userGateway).findByEmail(userEmail);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			saveProductUseCase.save(product, userEmail);
		});
		
		verify(userGateway).findByEmail(userEmail);
		
		verify(productDBGateway, never()).save(any(Product.class));
	}
	
	@Test
	public void update() {
		final String productId = "anyId";
		final String productName = "newName";
		final String description = "newDescription";
		final String barCode = "newBarCode";
		final String userId = "anyUserId";
		final String userEmail = "anyUserEmail";
		
		final Product productToUpdate = new Product(productId, productName, description, barCode, null);
		
		final User userToReturn = new User(userId, userEmail);
		Optional<User> userOp = Optional.of(userToReturn);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		saveProductUseCase.update(productToUpdate, userEmail);
		
		verify(userGateway).findByEmail(userEmail);
		verify(validateUseCase).validate(productId, userId);
		
		ArgumentCaptor<Product> productAC = ArgumentCaptor.forClass(Product.class);
		verify(productDBGateway).save(productAC.capture());
		
		Product productUpdated = productAC.getValue();
		
		assertEquals(productToUpdate.getId(), productUpdated.getId());
		assertEquals(productToUpdate.getName(), productUpdated.getName());
		assertEquals(productToUpdate.getDescription(), productUpdated.getDescription());
		assertEquals(productToUpdate.getBarCode(), productUpdated.getBarCode());
		assertEquals(userId, productUpdated.getUser().getId());
		assertEquals(userEmail, productUpdated.getUser().getEmail());
		assertNotNull(productUpdated.getUser());
	}
	
	@Test
	public void updateUserUnauthorized() {
		final String productId = "anyId";
		final String productName = "newName";
		final String description = "newDescription";
		final String barCode = "newBarCode";
		final String userId = "anyUserId";
		final String userEmail = "anyUserEmail";
		
		final Product productToUpdate = new Product(productId, productName, description, barCode, null);
		
		Optional<User> userOp = Optional.empty();
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			saveProductUseCase.update(productToUpdate, userEmail);
		});
		
		verify(userGateway).findByEmail(userEmail);
		verify(validateUseCase, never()).validate(productId, userId);
		verify(productDBGateway, never()).save(any(Product.class));
	}
}
