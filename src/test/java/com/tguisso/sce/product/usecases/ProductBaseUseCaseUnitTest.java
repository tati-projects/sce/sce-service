package com.tguisso.sce.product.usecases;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.user.UserGateway;
import com.tguisso.sce.product.usecases.exception.UserUnauthorizedBusinessException;

@ExtendWith(MockitoExtension.class)
public class ProductBaseUseCaseUnitTest {
	
	@InjectMocks
	private ProductBaseUseCase productBaseUseCase = new SaveProductUseCase();
	
	@Mock
	private UserGateway userGateway;
	
	@Test
	public void getUserByUserNameSuccess() {
		final String userId = "anyUserId";
		final String userEmail = "anyUserEmail";
		final User userToBeReturned = new User(userId, userEmail);
		
		final Optional<User> userOp = Optional.of(userToBeReturned);
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		User userReturned = productBaseUseCase.getUserByUserName(userEmail); 
		
		verify(userGateway).findByEmail(userEmail);
		
		assertEquals(userToBeReturned.getId(), userReturned.getId());
		assertEquals(userToBeReturned.getEmail(), userReturned.getEmail());
	}
	
	@Test
	public void getUserByUserNameUserUnauthorized() {
		final String userEmail = "anyUserEmail";
		
		final Optional<User> userOp = Optional.empty();
		doReturn(userOp).when(userGateway).findByEmail(userEmail);
		
		assertThrows(UserUnauthorizedBusinessException.class, () -> {
			productBaseUseCase.getUserByUserName(userEmail);
		});
		
		verify(userGateway).findByEmail(userEmail);
	}

}
