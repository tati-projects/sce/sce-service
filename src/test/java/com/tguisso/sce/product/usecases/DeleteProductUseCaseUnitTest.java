package com.tguisso.sce.product.usecases;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.gateway.stock.StockGateway;
import com.tguisso.sce.product.gateway.user.UserGateway;

@ExtendWith(MockitoExtension.class)
public class DeleteProductUseCaseUnitTest {
	
	@InjectMocks
	private DeleteProductUseCase deleteProductUseCase;
	
	@Mock
	private UserGateway userGateway;
	
	@Mock
	private ProductDBGateway productDBGateway;
	
	@Mock
	private ValidateUseCase validateUseCase;
	
	@Mock
	private GetProductUseCase getProductUseCase;
	
	@Mock
	private StockGateway stockGateway;
	
	@Test
	public void delete() {
		final String productId = "anyIdProduct";
		final String userName = "anyUserEmail";
		final String userId = "anyUserId";
		
		final User user = new User(userId, userName);
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userName);
		
		deleteProductUseCase.delete(productId, userName);
		
		verify(userGateway).findByEmail(userName);
		verify(validateUseCase).validate(productId, user.getId());
		verify(stockGateway).deleteByProductId(productId, userName);
		verify(productDBGateway).deleteById(productId);
	}
	
	@Test
	public void deleteByUser() {
		final String userName = "anyUserEmail";
		final String userId = "anyUserId";
		
		final User user = new User(userId, userName);
		Optional<User> userOp = Optional.of(user);
		doReturn(userOp).when(userGateway).findByEmail(userName);
		
		List<Product> products = new ArrayList<>();
		products.add(new Product(null, "productA", null, null, user));
		products.add(new Product(null, "productB", null, null, user));
		doReturn(products).when(getProductUseCase).getAll(userName);
		
		deleteProductUseCase.deleteByUser(userId, userName);
		verify(userGateway).findByEmail(userName);
		verify(getProductUseCase).getAll(userName);
		verify(productDBGateway).deleteAll(products);
	}
}
