package com.tguisso.sce.product.usecases;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.usecases.exception.ProductNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class ValidateUseCaseUnitTest {
	
	@InjectMocks
	private ValidateUseCase validateUseCase;
	
	@Mock
	private ProductDBGateway productDBGateway;
	
	@Test
	public void validateProduct() {
		final String productId = "anyId";
		final String productName = "anyName";
		final String userId = "anyUserId";
		final String userEmail = "anyEmail";
		final User user = new User(userId, userEmail);
		
		final Product product = new Product(productId, productName, null, null, user);
		Optional<Product> productOp = Optional.of(product);
		doReturn(productOp).when(productDBGateway).findByIdAndUserId(productId, userId);
		
		validateUseCase.validate(productId, userId);
		
		verify(productDBGateway).findByIdAndUserId(productId, userId);
		
		assertEquals(productId, productOp.get().getId());
		assertEquals(productName, productOp.get().getName());
		assertEquals(user.getId(), productOp.get().getUser().getId());
	}
	
	@Test
	public void validateWithNoProduct() {
		final String productId = "anyId";
		final String userId = "anyUserId";
		
		Optional<Product> productOp = Optional.empty();
		doReturn(productOp).when(productDBGateway).findByIdAndUserId(productId, userId);
		
		assertThrows(ProductNotFoundBusinessException.class, () -> {
			validateUseCase.validate(productId, userId);
		});
		
		verify(productDBGateway).findByIdAndUserId(productId, userId);
		
	}

}
