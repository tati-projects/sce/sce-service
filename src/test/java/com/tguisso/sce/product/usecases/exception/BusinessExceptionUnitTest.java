package com.tguisso.sce.product.usecases.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

public class BusinessExceptionUnitTest {
	
	@Test
	public void userUnauthorizedBusinessException() {
		SceBaseException exception = new UserUnauthorizedBusinessException();
		assertEquals("sce.product.user.userUnauthorized", exception.getCode());
		assertEquals("User unauthorized", exception.getMessage());
		assertEquals(HttpStatus.UNAUTHORIZED, exception.getHttpStatus());
	}
	
	@Test
	public void productNotFoundBusinessException() {
		SceBaseException exception = new ProductNotFoundBusinessException();
		assertEquals("sce.productNotFound", exception.getCode());
		assertEquals("Product not found.", exception.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
	}

}
