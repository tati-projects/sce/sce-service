package com.tguisso.sce.starter.exception.gateways.http.controllers.json;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class ExceptionJson {

	private final String code;
	private final String message;
	
	public ExceptionJson(final SceBaseException baseException) {
		this.code = baseException.getCode();
		this.message = baseException.getMessage();
	}
}
