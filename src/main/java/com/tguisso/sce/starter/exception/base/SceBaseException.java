package com.tguisso.sce.starter.exception.base;

import org.springframework.http.HttpStatus;

public abstract class SceBaseException extends RuntimeException {
	private static final long serialVersionUID = 273112930479290412L;

	public abstract String getCode();
	public abstract HttpStatus getHttpStatus();
	public abstract String getMessage();
}
