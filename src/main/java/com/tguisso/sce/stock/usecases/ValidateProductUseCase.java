package com.tguisso.sce.stock.usecases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.gateway.product.ProductGateway;
import com.tguisso.sce.stock.gateway.product.exception.ProductNotFoundBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ValidateProductUseCase {
	
	@Autowired
	private ProductGateway productGateway;

	public void validate(List<Item> items, String userName) {
		log.info("Start items={}, userName={}", items, userName);
		List<Product> products = productGateway.get(userName);
		for (Item item : items) {
			boolean idNotFound = true;
			for (Product product : products) {
				if(product.getId().equals(item.getProduct().getId())) {
					idNotFound = false;
					break;
				}
			}
			if(idNotFound == true) {
				log.warn("Product not found. items={}, userName={}", items, userName);
				throw new ProductNotFoundBusinessException();
			}
		}
		log.info("End items={}, userName={}", items, userName);
	}
}
