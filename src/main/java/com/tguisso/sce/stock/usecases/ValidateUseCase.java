package com.tguisso.sce.stock.usecases;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;
import com.tguisso.sce.stock.usecases.exception.StockNotFoundBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("validateUseCaseStock")
public class ValidateUseCase {
	
	@Autowired
	private StockDBGateway stockDBGateway;

	public void validate(String stockId, String userId) {
		log.info("Start stockId={}, userId={}", stockId, userId);
		Optional<Stock> stockOp = stockDBGateway.findByIdAndUserId(stockId, userId);
		if (stockOp.isEmpty()) {
			log.warn("Stock not found. stockId={}, userId={}", stockId, userId);
			throw new StockNotFoundBusinessException();
		}
		log.info("End stockId={}, userId={}", stockId, userId);
	}
}
