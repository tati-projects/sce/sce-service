package com.tguisso.sce.stock.usecases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeleteStockUseCase extends StockBaseUseCase {
	
	@Autowired
	private StockDBGateway stockDBGateway;
	
	@Autowired
	private ValidateUseCase validateUseCase;
	
	@Autowired
	private GetStockUseCase getStockUseCase;

	public void delete(String stockID, String userName) {
		log.info("Start stockID={}, userName={}", stockID, userName);
		User user = getUserByUserName(userName);
		validateUseCase.validate(stockID, user.getId());
		stockDBGateway.deleteById(stockID);
		log.info("End stockID={}, userName={}", stockID, userName);
	}
	
	public void deleteByUser(String userId, String userName) {
		log.info("Start userId={}, userName={}", userId, userName);
		List<Stock> stocks = getStockUseCase.getAll(userName);
		stockDBGateway.deleteAll(stocks);
		log.info("End userId={}, userName={}", userId, userName);
	}

}
