package com.tguisso.sce.stock.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class InvalidQuantityBusinessException extends SceBaseException {
	private static final long serialVersionUID = 913706692147848320L;
	
	private String code = "sce.stock.invalidQuantity";
	private String message = "Invalid Quantity";
	private HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;

}
