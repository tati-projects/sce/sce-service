package com.tguisso.sce.stock.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SaveStockUseCase extends StockBaseUseCase {
	
	@Autowired
	private StockDBGateway stockDBGateway;
		
	@Autowired
	private GetStockUseCase getStockUseCase;

	public String save(Stock stock, String userName) {
		log.info("Start stock={}, userName={}", stock, userName);
		User user = getUserByUserName(userName);
		Stock stockToSave = new Stock(stock.getId(), stock.getName(), stock.getItems(), user);
		log.info("End stockToSave={}, userName={}", stockToSave, userName);
		return stockDBGateway.save(stockToSave);
	}
	
	public void update(Stock stockToUpdate, String userName) {
		log.info("Start stockToUpdate={}, userName={}", stockToUpdate, userName);
		Stock stockExistent = getStockUseCase.get(stockToUpdate.getId(), userName);
		Stock stockToSave = new Stock(
				stockToUpdate.getId(), 
				stockToUpdate.getName(), 
				stockExistent.getItems(), 
				stockExistent.getUser());
		stockDBGateway.save(stockToSave);
		log.info("End stockToSave={}, userName={}", stockToSave, userName);
	}

}
