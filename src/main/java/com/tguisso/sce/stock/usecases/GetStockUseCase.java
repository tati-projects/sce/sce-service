package com.tguisso.sce.stock.usecases;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GetStockUseCase extends StockBaseUseCase {
	
	@Autowired
	private StockDBGateway stockDBGateway;
	
	@Autowired
	private ValidateUseCase validateUseCase;

	public Stock get(String id, String userName) {
		log.info("Start id={}, userName={}", id, userName);
		User user = getUserByUserName(userName);
		validateUseCase.validate(id, user.getId());
		Optional<Stock> stockOp = stockDBGateway.findByIdAndUserId(id, user.getId());
		log.info("End id={}, userName={}", id, userName);
		return stockOp.get();
	}

	public List<Stock> getAll(String userName) {
		log.info("Start userName={}", userName);
		User user = getUserByUserName(userName);
		log.info("End userName={}", userName);
		return stockDBGateway.findAllByUserId(user.getId());
	}
	
}
