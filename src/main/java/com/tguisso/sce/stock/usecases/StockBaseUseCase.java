package com.tguisso.sce.stock.usecases;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.user.UserGateway;
import com.tguisso.sce.stock.usecases.exception.UserUnauthorizedBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class StockBaseUseCase {
	
	@Autowired
	private UserGateway userGateway;
	
	public User getUserByUserName(String userName) {
		log.info("Start userName={}", userName);
		Optional<User> userOp = userGateway.findByEmail(userName);
		if(userOp.isPresent()) {
			final User userFound = userOp.get();
			log.info("End userFound={}", userFound);
			return userFound;
		}
		log.warn("User unauthorized: {}", userName);
		throw new UserUnauthorizedBusinessException();
	}

}
