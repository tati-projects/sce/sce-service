package com.tguisso.sce.stock.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class UserUnauthorizedBusinessException extends SceBaseException {
	private static final long serialVersionUID = 4444173125761140558L;
	
	private String code = "sce.stock.user.userUnauthorized";
	private String message = "User unauthorized";
	private HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;

}
