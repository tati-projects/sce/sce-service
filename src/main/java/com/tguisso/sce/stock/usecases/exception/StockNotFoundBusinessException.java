package com.tguisso.sce.stock.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class StockNotFoundBusinessException extends SceBaseException {
	private static final long serialVersionUID = 1517895498086775899L;
	
	private String code = "sce.stock.stockNotFound";
	private String message = "Stock Not Found";
	private HttpStatus httpStatus = HttpStatus.NOT_FOUND;

}
