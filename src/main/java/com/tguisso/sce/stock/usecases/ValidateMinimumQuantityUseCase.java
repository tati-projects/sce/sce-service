package com.tguisso.sce.stock.usecases;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.usecases.exception.InvalidQuantityBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ValidateMinimumQuantityUseCase {
	
	public void validateMinimumQuantity(List<Item> items) {
		log.info("Start items={}", items);
		for (Item item : items) {
			if(item.getQuantity() < 0) {
				log.warn("Invalid quantity. {}", item.getQuantity());
				throw new InvalidQuantityBusinessException();
			}
		}
		log.info("End items={}", items);
	}

}
