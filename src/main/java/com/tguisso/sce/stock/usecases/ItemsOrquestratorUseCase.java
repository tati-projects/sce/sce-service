package com.tguisso.sce.stock.usecases;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ItemsOrquestratorUseCase {
	
	@Autowired
	private GetStockUseCase getStockUseCase;
	
	@Autowired
	private StockDBGateway stockDBGateway;
	
	@Autowired
	private ValidateProductUseCase validateProductUseCase;
	
	@Autowired
	private ValidateMinimumQuantityUseCase validateMinimumQuantityUseCase;

	public void changeItems(Stock stock, String userName) {
		log.info("Start stock={}, userName={}", stock, userName);
		Stock stockToSaveItems = getStockUseCase.get(stock.getId(), userName);
		validateProductUseCase.validate(stock.getItems(), userName);
		validateMinimumQuantityUseCase.validateMinimumQuantity(stock.getItems());
		removeZeroQuantity(stock);
		stockToSaveItems.setItems(stock.getItems());
		stockDBGateway.save(stockToSaveItems);
		log.info("End stock={}, userName={}", stock, userName);
	}
	
	public void deleteItemByProductId(String productId, String userName) {
		log.info("Start productId={}, userName={}", productId, userName);
		List<Stock> stocks = getStockUseCase.getAll(userName);
		removeSameProductId(productId, stocks);
		stockDBGateway.saveAll(stocks);
		log.info("End productId={}, userName={}", productId, userName);
	}

	private void removeZeroQuantity(Stock stock) {
		Iterator<Item> iterator = stock.getItems().iterator();
		while(iterator.hasNext()) {
			if(iterator.next().getQuantity().equals(0)) {
				iterator.remove();
			}
		}
	}
	
	private void removeSameProductId(String productId, List<Stock> stocks) {
		for (Stock stock : stocks) {
			Iterator<Item> iterator = stock.getItems().iterator();
			while(iterator.hasNext()) {
				System.out.println();
				final Product product = iterator.next().getProduct(); 
				System.out.println(product.getId());
				if(product.getId().equals(productId)) {
					iterator.remove();
				}
			}
		}
	}

}
