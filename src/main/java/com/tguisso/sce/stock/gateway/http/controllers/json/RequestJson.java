package com.tguisso.sce.stock.gateway.http.controllers.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class RequestJson {
	private String stockId;
	List<ItemJson> items;
}
