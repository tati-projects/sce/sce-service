package com.tguisso.sce.stock.gateway.user.rest;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.stock.domains.User;
import com.tguisso.sce.stock.gateway.user.UserGateway;
import com.tguisso.sce.stock.gateway.user.exception.ErrorToAccessUserServiceGatewayException;
import com.tguisso.sce.stock.gateway.user.exception.UserNotFoundGatewayException;
import com.tguisso.sce.user.gateways.http.controllers.UserController;
import com.tguisso.sce.user.gateways.http.controllers.json.UserJson;
import com.tguisso.sce.user.usecases.exception.UserNotFoundBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("userDirectAccessToUserStock")
public class UserDirectAccessToUser implements UserGateway {
	
	@Autowired
	UserController userController;

	@Override
	public Optional<User> findByEmail(final String email) {
		try {
			log.info("Start email={}", email);
			UserJson userJson = userController.getByEmail(email);
			User userFound = mapperJsonToUser(userJson);
			log.info("End userFound={}", userFound);
			return Optional.of(userFound);
		} catch (UserNotFoundBusinessException e) {
			log.warn("User not found: {}", email);
			throw new UserNotFoundGatewayException();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			throw new ErrorToAccessUserServiceGatewayException();
		}
	}

	private User mapperJsonToUser(UserJson userJson) {
		User user = new User(userJson.getId(), userJson.getEmail());
		return user;
	}

}
