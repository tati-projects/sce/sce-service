package com.tguisso.sce.stock.gateway.product.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ProductNotFoundBusinessException extends SceBaseException {
	private static final long serialVersionUID = -1736736850871062670L;
	
	private String code = "sce.stock.productNotFound";
	private String message = "Product not found.";
	private HttpStatus httpStatus = HttpStatus.NOT_FOUND;
}
