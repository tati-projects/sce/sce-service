package com.tguisso.sce.stock.gateway.database.mongo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.gateway.database.StockDBGateway;
import com.tguisso.sce.stock.gateway.database.exception.ErrorToAccessDatabaseGatewayException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StockMongoGateway implements StockDBGateway {
	
	@Autowired
	private StockRepository stockRepository;

	@Override
	public String save(Stock stock) {
		try {
			log.info("Start stock={}", stock);
			String generatedStockId = stockRepository.save(stock).getId();
			log.info("End generatedStockId={}", generatedStockId);
			return generatedStockId;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public Optional<Stock> findByIdAndUserId(String id, String userId) {
		try {
			log.info("Start id={}, userId={}", id, userId);
			Optional<Stock> stockFound = stockRepository.findByIdAndUserId(id, userId);
			log.info("End stockFound={}", stockFound);
			return stockFound;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public List<Stock> findAllByUserId(String userId) {
		try {
			log.info("Start userId={}", userId);
			List<Stock> stocksFound = stockRepository.findAllByUserId(userId);
			log.info("End stocksFound={}", stocksFound);
			return stocksFound;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}


	@Override
	public void saveAll(List<Stock> stocks) {
		try {
			log.info("Start stocks={}", stocks);
			stockRepository.saveAll(stocks);
			log.info("End stocks={}", stocks);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public void deleteById(String id) {
		try {
			log.info("Start id={}", id);
			stockRepository.deleteById(id);
			log.info("End id={}", id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public void deleteAll(List<Stock> stocks) {
		try {
			stockRepository.deleteAll(stocks);
		} catch (Exception e) {
			throw new ErrorToAccessDatabaseGatewayException();
		}
		
	}
	
}
