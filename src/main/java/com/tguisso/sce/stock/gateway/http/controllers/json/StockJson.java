package com.tguisso.sce.stock.gateway.http.controllers.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class StockJson {
	private String id;
	private String name;
	private List<ItemJson> itemsJson;
	private UserJson userJson;
	
}
