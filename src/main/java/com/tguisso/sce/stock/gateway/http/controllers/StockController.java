package com.tguisso.sce.stock.gateway.http.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tguisso.sce.stock.domains.Item;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.domains.Stock;
import com.tguisso.sce.stock.gateway.http.controllers.json.ItemJson;
import com.tguisso.sce.stock.gateway.http.controllers.json.RequestJson;
import com.tguisso.sce.stock.gateway.http.controllers.json.StockJson;
import com.tguisso.sce.stock.gateway.http.controllers.json.UserJson;
import com.tguisso.sce.stock.usecases.ItemsOrquestratorUseCase;
import com.tguisso.sce.stock.usecases.DeleteStockUseCase;
import com.tguisso.sce.stock.usecases.GetStockUseCase;
import com.tguisso.sce.stock.usecases.SaveStockUseCase;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("stock")
public class StockController {
	
	@Autowired
	private SaveStockUseCase saveStockUseCase;
	
	@Autowired
	private GetStockUseCase getStockUseCase;
	
	@Autowired
	private DeleteStockUseCase deleteStockUseCase;
	
	@Autowired
	private ItemsOrquestratorUseCase itemsOrquestratorUseCase;
	
	@PostMapping
	public String create(
			@RequestBody(required = true) final StockJson stockJson,
			@RequestHeader(name = "logged-in-user-name", required = true) String userName) {
		log.info("Start stockJson={}, userName={}", stockJson, userName);
		Stock stock = mapperJsonToStock(stockJson);
		String generatedStockId = saveStockUseCase.save(stock, userName);
		log.info("End generatedStockId={}", generatedStockId);
		return generatedStockId;
	}
	
	@PutMapping
	public void update(
			@RequestBody(required = true) final StockJson stockJson, 
			@RequestHeader(name = "logged-in-user-name", required = true)String userName) {
		log.info("Start stockJson={}, userName={}", stockJson, userName);
		Stock stock = mapperJsonToStock(stockJson);
		saveStockUseCase.update(stock, userName);
		log.info("End stockJson={}, userName={}", stockJson, userName);
	}
	
	@GetMapping("{id}")
	public StockJson get(
			@PathVariable(name = "id", required = true) final String id, 
			@RequestHeader(name = "logged-in-user-name", required = true)String userName) {
		log.info("Start id={}, userName={}", id, userName);
		Stock stock = getStockUseCase.get(id, userName);
		StockJson stockJsonReturned = mapperStockToJson(stock);
		log.info("End stockJsonReturned={}", stockJsonReturned);
		return stockJsonReturned;
	}
	
	@GetMapping()
	public List<StockJson> getAll(
			@RequestHeader(name = "logged-in-user-name", required = true)String userName) {
		log.info("Start userName={}", userName);
		List<Stock> stocks = getStockUseCase.getAll(userName);
		log.info("End userName={}", userName);
		return mapperStockListToJson(stocks);
	}
	
	@PatchMapping("/update/item")
	public void updateItem(
			@RequestBody(required = true) final RequestJson requestJson ,
			@RequestHeader(name = "logged-in-user-name", required = true) String userName) {
		log.info("Start requestJson={}, userName={}", requestJson, userName);
		List<Item> items = mapperItemsJsonToItems(requestJson);
		Stock stock = new Stock(requestJson.getStockId(), null, items, null); 
		itemsOrquestratorUseCase.changeItems(stock, userName);
		log.info("End requestJson={}, userName={}", requestJson, userName);
	}

	@DeleteMapping("{id}")
	public void delete(
			@PathVariable(name = "id", required = true) final String id,
			@RequestHeader(name = "logged-in-user-name", required = true) String userName) {
		log.info("Start id={}, userName={}", id, userName);
		deleteStockUseCase.delete(id, userName);
		log.info("End id={}, userName={}", id, userName);
	}
		
	@DeleteMapping("/products/{productId}")
	public void deleteByProductId(
			@PathVariable(name = "productId", required = true) final String productId,
			@RequestHeader(name = "logged-in-user-name", required = true) String userName) {
		log.info("Start productId={}, userName={}", productId, userName);
		itemsOrquestratorUseCase.deleteItemByProductId(productId, userName);
		log.info("End productId={}, userName={}", productId, userName);
	}
	
	@DeleteMapping("{userId}")
	public void deleteByUser(
			@PathVariable(name = "userId", required = true) final String userId,
			@RequestHeader(name = "logged-in-user-name", required = true) String userName) {
		log.info("Start userId={}, userName={}", userId, userName);
		deleteStockUseCase.deleteByUser(userId, userName);
		log.info("End userId={}, userName={}", userId, userName);
	}
	
	private Stock mapperJsonToStock(final StockJson stockJson) {
		List<Item> items = new ArrayList<>();
		return new Stock(stockJson.getId(), stockJson.getName(), items, null);
	}
	
	private StockJson mapperStockToJson(final Stock stock) {
		List<ItemJson> itemsJson = new ArrayList<>();
		for (Item item : stock.getItems()) {
			itemsJson.add(new ItemJson(item.getQuantity(), item.getProduct().getId()));
		}
		UserJson userJson = new UserJson(stock.getUser().getId(), stock.getUser().getEmail());
		return new StockJson(stock.getId(), stock.getName(), itemsJson, userJson);
	}
	
	private List<Item> mapperItemsJsonToItems(final RequestJson requestJson) {
		List<Item> items = new ArrayList<>();
		for (ItemJson itemJson : requestJson.getItems()) {
			Product product = new Product(itemJson.getProductId());
			items.add(new Item(itemJson.getQuantity(), product));
		}
		return items;
	}
	
	private List<StockJson> mapperStockListToJson(final List<Stock> stocks) {
		List<StockJson> stocksJson = new ArrayList<>();
		for (Stock stock : stocks) {
			stocksJson.add(this.mapperStockToJson(stock));
		}
		return stocksJson;		
	}
	
}
