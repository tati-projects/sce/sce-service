package com.tguisso.sce.stock.gateway.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductJson {
	private String id;
}
