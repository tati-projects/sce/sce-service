package com.tguisso.sce.stock.gateway.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ItemJson {
	private Integer quantity;
	private String productId;
}
