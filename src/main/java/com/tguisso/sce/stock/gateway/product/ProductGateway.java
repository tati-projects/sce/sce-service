package com.tguisso.sce.stock.gateway.product;

import java.util.List;

import com.tguisso.sce.stock.domains.Product;

public interface ProductGateway {
	
	public List<Product> get(String userName);

}
