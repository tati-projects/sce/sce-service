package com.tguisso.sce.stock.gateway.product.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ErrorToAccessProductGatewayException extends SceBaseException {
	private static final long serialVersionUID = 8297114106946852228L;
	
	private String code = "sce.stock.product.errorToAccessProductService";
	private String message = "Error to access product service";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
