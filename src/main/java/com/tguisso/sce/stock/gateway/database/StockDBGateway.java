package com.tguisso.sce.stock.gateway.database;

import java.util.List;
import java.util.Optional;

import com.tguisso.sce.stock.domains.Stock;

public interface StockDBGateway {
	
	String save(Stock stock);
	void saveAll(List<Stock> stocks);
	Optional<Stock> findByIdAndUserId(String id, String userId);
	List<Stock> findAllByUserId(String userId);
	void deleteById(String id);
	void deleteAll(List<Stock> stocks);
}
