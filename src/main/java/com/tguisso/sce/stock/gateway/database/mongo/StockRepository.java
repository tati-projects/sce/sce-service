package com.tguisso.sce.stock.gateway.database.mongo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.tguisso.sce.stock.domains.Stock;

@Repository
public interface StockRepository extends MongoRepository<Stock, String>{

	Optional<Stock> findByIdAndUserId(String id, String userId);

	List<Stock> findAllByUserId(String userId);

}
