package com.tguisso.sce.stock.gateway.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserJson {
	private String id;
	private String email;
}
