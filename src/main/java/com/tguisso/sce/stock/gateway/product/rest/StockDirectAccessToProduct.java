package com.tguisso.sce.stock.gateway.product.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.product.gateway.http.controllers.ProductController;
import com.tguisso.sce.product.gateway.http.controllers.json.ProductJson;
import com.tguisso.sce.stock.domains.Product;
import com.tguisso.sce.stock.gateway.product.ProductGateway;
import com.tguisso.sce.stock.gateway.product.exception.ErrorToAccessProductGatewayException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StockDirectAccessToProduct implements ProductGateway {
	
	@Autowired
	private ProductController productController;

	@Override
	public List<Product> get(String userName) {
		try {
			log.info("Start userName={}", userName);
			List<ProductJson> productsJson = productController.getAll(userName);
			List<Product> productsReturned = mapperJsonToProduct(productsJson);
			log.info("End productsReturned={}", productsReturned);
			return productsReturned;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			throw new ErrorToAccessProductGatewayException();
		}
		
	}

	private List<Product> mapperJsonToProduct(List<ProductJson> productsJson) {
		List<Product> products = new ArrayList<>();
		for (ProductJson productJson : productsJson) {
			products.add(new Product(productJson.getId()));
		}
		return products;
	}

}
