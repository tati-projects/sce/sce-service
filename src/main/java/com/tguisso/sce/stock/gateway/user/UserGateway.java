package com.tguisso.sce.stock.gateway.user;

import java.util.Optional;

import com.tguisso.sce.stock.domains.User;

public interface UserGateway {
	Optional<User> findByEmail(String email);
}
