package com.tguisso.sce.stock.gateway.database.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ErrorToAccessDatabaseGatewayException extends SceBaseException{
	private static final long serialVersionUID = 8768471593536338454L;
	
	private String code = "sce.errorToAccessDatabase";
	private String message = "Error to access database";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
