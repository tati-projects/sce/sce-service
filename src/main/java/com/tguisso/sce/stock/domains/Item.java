package com.tguisso.sce.stock.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Item {
	private Integer quantity;
	private Product product;
}
