package com.tguisso.sce.stock.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class User {
	private String id;
	private String email;
}
