package com.tguisso.sce.stock.domains;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Stock {
	private String id;
	private String name;
	private List<Item> items;
	private User user;
	
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
}
