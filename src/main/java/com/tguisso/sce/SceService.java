package com.tguisso.sce;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SceService {
    public static void main(String[] args) throws Exception {
    	Locale.setDefault(new Locale( "pt", "BR" ));
        SpringApplication.run(SceService.class, args);
    }
}
