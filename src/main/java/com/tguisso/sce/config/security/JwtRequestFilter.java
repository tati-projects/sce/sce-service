package com.tguisso.sce.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.tguisso.sce.starter.token.jwt.JwtTokenUtil;
import com.tguisso.sce.starter.token.jwt.SceUserDetailsService;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JwtRequestFilter extends OncePerRequestFilter {
	
	@Autowired
	private SceUserDetailsService sceUserDetailsService;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		final AddParamsToHeader requestWrapper = new AddParamsToHeader(request);
		final String requestTokenHeader = request.getHeader("sce-user-token");
		String username = null;
		String jwtToken = null;
		
		log.info("Start doFilterInternal");
		
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			try {
				//TODO: verificar se vale a pena adicionar userName no cabeçalho para ser passado pros user cases.
				// Ou se é melhor fazer logo uma chamada daqui para o user case de 'possui autorização'
				username = jwtTokenUtil.getUsernameFromToken(jwtToken);
				requestWrapper.addHeader("logged-in-user-name", username);
				
			} catch (IllegalArgumentException e) {
				log.error("Unable to get JWT Token, {}", requestTokenHeader);				
			} catch (ExpiredJwtException e) {
				log.error("JWT Token has expired, {}", requestTokenHeader);
			}
		} else {
			log.error("JWT Token does not begin with Bearer String, {}", requestTokenHeader);
		}
		
		
		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = this.sceUserDetailsService.loadUserByUsername(username);			
			if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));				
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}
		
		log.info("End doFilterInternal");
		chain.doFilter(requestWrapper, response);
	}
}