package com.tguisso.sce.product.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ProductNotFoundBusinessException extends SceBaseException {
	private static final long serialVersionUID = -8766547662246948747L;
	
	private String code = "sce.productNotFound";
	private String message = "Product not found.";
	private HttpStatus httpStatus = HttpStatus.NOT_FOUND;

}
