package com.tguisso.sce.product.usecases;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.usecases.exception.ProductNotFoundBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("validateUseCaseProduct")
public class ValidateUseCase {
	
	@Autowired
	private ProductDBGateway productDBGateway;

	public void validate(String id, String userId) {
		log.info("Start id={}, userId={}", id, userId);

		Optional<Product> productOp = productDBGateway.findByIdAndUserId(id, userId);
		if (productOp.isEmpty()) {
			log.warn("Product not found. id={}, userId={}", id, userId);
			throw new ProductNotFoundBusinessException();
		}
		log.info("End productOp={}", productOp);
	}
}
