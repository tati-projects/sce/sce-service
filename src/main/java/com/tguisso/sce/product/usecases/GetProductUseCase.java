package com.tguisso.sce.product.usecases;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GetProductUseCase extends ProductBaseUseCase {
	
	@Autowired
	private ProductDBGateway productDBGateway;
	
	@Autowired
	private ValidateUseCase validateUseCase;
	
	public Product get(String productId, String userName) {
		log.info("Start productId={}, userName={}", productId, userName);
		User user = getUserByUserName(userName);
		validateUseCase.validate(productId, user.getId());
		
		Optional<Product> productFoundOp = productDBGateway.findByIdAndUserId(productId, user.getId()); 
		log.info("End productFoundOp={}", productFoundOp);
		
		return productFoundOp.get();
	}
	
	public List<Product> getAll(String userName) {
		log.info("Start userName={}", userName);
		User user = getUserByUserName(userName);
		
		List<Product> productsFound = productDBGateway.findAllByUserId(user.getId()); 
		log.info("End productsFound={}", productsFound);
		
		return productsFound;
	}

}
