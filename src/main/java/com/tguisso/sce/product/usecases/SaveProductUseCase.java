package com.tguisso.sce.product.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SaveProductUseCase extends ProductBaseUseCase {

	@Autowired
	private ProductDBGateway productDBGateway;
	
	@Autowired
	private ValidateUseCase validateUseCase;

	public String save(Product product, String userName) {
		log.info("Start product={}, userName={}", product, userName);
		User user = getUserByUserName(userName);
		Product productToSave = createProductToSave(product, user);
		
		final String generatedProductId = productDBGateway.save(productToSave);
		
		log.info("End generatedProductId={}", generatedProductId);
		
		return generatedProductId; 
	}
	
	public void update(Product product, String userName) {
		log.info("Start product={}, userName={}", product, userName);
		User user = getUserByUserName(userName);
		validateUseCase.validate(product.getId(), user.getId());
		Product productToSave = createProductToSave(product, user);
		productDBGateway.save(productToSave); 
		log.info("End product={}, userName={}, productToSave={}", product, userName, productToSave);
	}
	
	private Product createProductToSave(Product product, User user) {
		Product productToSave = new Product(
				product.getId(), product.getName(), product.getDescription(), 
				product.getBarCode(), user);
		return productToSave;
	}
}
