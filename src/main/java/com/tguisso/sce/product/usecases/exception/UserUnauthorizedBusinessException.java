package com.tguisso.sce.product.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class UserUnauthorizedBusinessException extends SceBaseException{
	private static final long serialVersionUID = -5741837987343397306L;

	private String code = "sce.product.user.userUnauthorized";
	private String message = "User unauthorized";
	private HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
}
