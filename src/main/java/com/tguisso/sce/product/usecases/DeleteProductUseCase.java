package com.tguisso.sce.product.usecases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.domains.User;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.gateway.stock.StockGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeleteProductUseCase extends ProductBaseUseCase {
	
	@Autowired
	private ProductDBGateway productDBGateway;
	
	@Autowired
	private ValidateUseCase validateUseCase;
	
	@Autowired
	private GetProductUseCase getProductUseCase;
	
	@Autowired
	private StockGateway stockGateway;

	public void delete(String productId, String userName) {
		log.info("Start productId={}, userName={}", productId, userName);
		User user = getUserByUserName(userName);
		validateUseCase.validate(productId, user.getId());
		stockGateway.deleteByProductId(productId, userName);
		productDBGateway.deleteById(productId);
		log.info("End productId={}, userName={}", productId, userName);
	}
	
	public void deleteByUser(String userId, String userName) {
		log.info("Start userId={}, userName={}", userId, userName);
		User user = getUserByUserName(userName);
		List<Product> productsToDelete = getProductUseCase.getAll(user.getEmail());
		productDBGateway.deleteAll(productsToDelete);
		log.info("End userId={}, userName={}, productsToDelete={}", userId, userName, productsToDelete);
	}

}
