package com.tguisso.sce.product.gateway.http.controllers.json;

import com.tguisso.sce.product.domains.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class ProductJson {
	private String id;
	private String name;
	private String description;
	private String barCode;
	private User user;//FIXME: mudar para "UserJson"
}
