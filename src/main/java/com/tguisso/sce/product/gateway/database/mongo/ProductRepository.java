package com.tguisso.sce.product.gateway.database.mongo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.tguisso.sce.product.domains.Product;

@Repository
public interface ProductRepository extends MongoRepository<Product, String>{

	Optional<Product> findByIdAndUserId(String id, String userId);

	List<Product> findAllByUserId(String userId);

}
