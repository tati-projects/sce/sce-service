package com.tguisso.sce.product.gateway.database;

import java.util.List;
import java.util.Optional;

import com.tguisso.sce.product.domains.Product;

public interface ProductDBGateway {
	
	String save(Product product);
	Optional<Product> findByIdAndUserId(String id, String userId);
	List<Product> findAllByUserId(String userId);
	void deleteById(String id);
	void deleteAll(List<Product> products);

}
