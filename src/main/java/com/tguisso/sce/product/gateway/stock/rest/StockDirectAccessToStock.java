package com.tguisso.sce.product.gateway.stock.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.product.gateway.stock.StockGateway;
import com.tguisso.sce.product.gateway.stock.exception.ErrorToAccessStockServiceGatewayException;
import com.tguisso.sce.stock.gateway.http.controllers.StockController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StockDirectAccessToStock implements StockGateway {
	
	@Autowired
	private StockController stockController;

	@Override
	public void deleteByProductId(String productId, String userName) {
		try {
			log.info("Start productId={}, userName={}", productId, userName);
			stockController.deleteByProductId(productId, userName);
			log.info("End productId={}, userName={}", productId, userName);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			throw new ErrorToAccessStockServiceGatewayException();
		}
	}
}
