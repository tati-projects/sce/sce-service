package com.tguisso.sce.product.gateway.stock.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ErrorToAccessStockServiceGatewayException extends SceBaseException {
	private static final long serialVersionUID = -1933101931720496157L;
	
	private String code = "sce.product.stock.errorToAccessStockService";
	private String message = "Error to access stock service";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
