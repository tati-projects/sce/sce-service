package com.tguisso.sce.product.gateway.user.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class UserNotFoundGatewayException extends SceBaseException {
	private static final long serialVersionUID = -7705097412182890423L;
	
	private String code = "sce.product.user.userNotFoundGateway";
	private String message = "User not Found";
	private HttpStatus httpStatus = HttpStatus.NOT_FOUND;

}
