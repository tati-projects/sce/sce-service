package com.tguisso.sce.product.gateway.user.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ErrorToAccessUserServiceGatewayException extends SceBaseException {
	private static final long serialVersionUID = -2283264439251029022L;
	
	private String code = "sce.product.user.errorToAccessUserService";
	private String message = "Error to access user service";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
