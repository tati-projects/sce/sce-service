package com.tguisso.sce.product.gateway.http.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.gateway.http.controllers.json.ProductJson;
import com.tguisso.sce.product.usecases.DeleteProductUseCase;
import com.tguisso.sce.product.usecases.GetProductUseCase;
import com.tguisso.sce.product.usecases.SaveProductUseCase;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("products")
public class ProductController {
	
	@Autowired
	private SaveProductUseCase saveProductUseCase;
	
	@Autowired
	private GetProductUseCase getProductUseCase;
	
	@Autowired
	private DeleteProductUseCase deleteProductUseCase;
	
	@PostMapping
	public String create(
			@RequestBody(required = true) final ProductJson productJson, 
			@RequestHeader(name = "logged-in-user-name", required = true) String userName) {
		log.info("Start productJson={}, userName={}", productJson, userName);
		
		Product product = mapperJsonToProduct(productJson);
		
		final String genereatedProductId = saveProductUseCase.save(product, userName); 
		
		log.info("End genereatedProductId={}", genereatedProductId);
		
		return genereatedProductId;
	}
	
	@PutMapping
	public void update(
			@RequestBody(required = true) final ProductJson productJson, 
			@RequestHeader(name = "logged-in-user-name", required = true)String userName) {
		log.info("Start productJson={}, userName={}", productJson, userName);
		Product product = mapperJsonToProduct(productJson);
		saveProductUseCase.update(product, userName);
		log.info("End product={}, userName={}", product, userName);
	}
	
	@GetMapping("{id}")
	public ProductJson get(
			@PathVariable(name = "id", required = true) final String productId, 
			@RequestHeader(name = "logged-in-user-name", required = true)String userName) {
		log.info("Start productId={}, userName={}", productId, userName);
		Product product = getProductUseCase.get(productId, userName);
		log.info("End productId={}, userName={}", productId, userName);
		return mapperProductToJson(product);
	}
	
	@GetMapping()
	public List<ProductJson> getAll(
			@RequestHeader(name = "logged-in-user-name", required = true)String userName) {
		log.info("Start userName={}", userName);
		List<Product> products = getProductUseCase.getAll(userName);
		log.info("End products={}", products);
		return mapperProductListToJson(products);
	}
	
	@DeleteMapping("{id}")
	public void delete(
			@PathVariable(name = "id", required = true) final String productId, 
			@RequestHeader(name = "logged-in-user-name", required = true)String userName) {
		log.info("Start productId={}, userName={}", productId, userName);
		deleteProductUseCase.delete(productId, userName);
		log.info("End productId={}, userName={}", productId, userName);
	}
	
	@DeleteMapping("/user/{userId}")
	public void deleteByUser(
			@PathVariable(name = "userId", required = true) final String userId,
			@RequestHeader(name = "logged-in-user-name", required = true) String userName) {
		log.info("Start userId={}, userName={}", userId, userName);
		deleteProductUseCase.deleteByUser(userId, userName);
		log.info("End userId={}, userName={}", userId, userName);
	}
	
	private Product mapperJsonToProduct(ProductJson productJson) {
		return new Product(
				productJson.getId(), 
				productJson.getName(), 
				productJson.getDescription(), 
				productJson.getBarCode(), 
				productJson.getUser());
	}
	
	private ProductJson mapperProductToJson(Product product) {
		return new ProductJson(
				product.getId(), 
				product.getName(), 
				product.getDescription(), 
				product.getBarCode(), 
				product.getUser());
	}
	
	private List<ProductJson> mapperProductListToJson(List<Product> products) {
		List<ProductJson> productsJson = new ArrayList<>();
		for (Product product : products) {
			productsJson.add(mapperProductToJson(product));
		}
		return productsJson;
	}

}
