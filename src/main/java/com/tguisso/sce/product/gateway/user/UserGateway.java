package com.tguisso.sce.product.gateway.user;

import java.util.Optional;

import com.tguisso.sce.product.domains.User;

public interface UserGateway {
	Optional<User> findByEmail(String email);
}
