package com.tguisso.sce.product.gateway.database.mongo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.product.domains.Product;
import com.tguisso.sce.product.gateway.database.ProductDBGateway;
import com.tguisso.sce.product.gateway.database.exception.ErrorToAccessDatabaseGatewayException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProductMongoGateway implements ProductDBGateway {
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	public String save(Product product) {
		try {
			log.info("Start product={}", product);
			Product p = productRepository.save(product);
			
			final String generatedProductId = p.getId(); 
			
			log.info("End generatedProductId={}", generatedProductId);
			
			return generatedProductId;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public void deleteById(String id) {
		try {
			log.info("Start id={}", id);
			productRepository.deleteById(id);
			log.info("End id={}", id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public Optional<Product> findByIdAndUserId(String id, String userId) {
		try {
			log.info("Start id={}, userId={}", id, userId);
			final Optional<Product> productFoundOp = productRepository.findByIdAndUserId(id, userId); 
			
			log.info("End productFoundOp={}", productFoundOp);
			
			return productFoundOp;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public List<Product> findAllByUserId(String userId) {
		try {
			log.info("Start userId={}", userId);
			
			final List<Product> productsFound = productRepository.findAllByUserId(userId);
			log.info("End productsFound={}", productsFound);
			
			return productsFound;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
		
	}

	@Override
	public void deleteAll(List<Product> products) {
		try {
			log.info("Start products={}", products);
			productRepository.deleteAll(products);
			log.info("End products={}", products);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
		
	}

}
