package com.tguisso.sce.product.gateway.stock;

public interface StockGateway {
	void deleteByProductId(String productId, String userName);
}
