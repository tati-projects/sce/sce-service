package com.tguisso.sce.product.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class Product {
	private String id;
	private String name;
	private String description;
	private String barCode;
	private User user;
}
