package com.tguisso.sce.product.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class User {
	private String id;
	private String email;
}
