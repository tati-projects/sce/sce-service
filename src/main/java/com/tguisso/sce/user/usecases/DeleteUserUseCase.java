package com.tguisso.sce.user.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.gateways.product.UserProductGateway;
import com.tguisso.sce.user.gateways.stock.StockGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeleteUserUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway;
	
	@Autowired
	private StockGateway stockGateway;
	
	@Autowired
	private UserProductGateway userProductGateway;
	
	@Autowired
	private UserAuthorizationUseCase userAuthorizationUseCase;
	
	public void delete(String id, String userName) {
		log.info("Start id={}, userName={}", id, userName);
		userAuthorizationUseCase.authorize(id, userName);
		userProductGateway.deleteProductByUserId(id, userName);
		stockGateway.deleteStockByUserId(id, userName);
		userDBGateway.deleteById(id);
		log.info("End id={}, userName={}", id, userName);
	}
	
}
