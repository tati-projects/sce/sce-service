package com.tguisso.sce.user.usecases;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.usecases.exception.UserUnauthorizedBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserAuthorizationUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway; 
	
	public void authorize(String id, String userEmail) {
		log.info("Start id={}, userEmail={}", id, userEmail);
		Optional<User> userOp = userDBGateway.findByEmail(userEmail);
		if(userOp.isEmpty() || !userOp.get().getId().equals(id)) {
			log.warn("User unauthorized: id={}, userEmail={}", id, userEmail);
			throw new UserUnauthorizedBusinessException();
		}
		log.info("End id={}, userEmail={}", id, userEmail);
	}
}
