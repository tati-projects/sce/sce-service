package com.tguisso.sce.user.usecases;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.usecases.exception.UserNotFoundBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GetUserUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway; 
	
	public User get(String id) {
		log.info("Start id={}", id);
		Optional<User> userOp = userDBGateway.findById(id);
		log.info("End id={}", id);
		return prepareUserToReturn(userOp);
	}
	
	public User getByEmail(String email) {
		log.info("Start email={}", email);
		Optional<User> userOp = userDBGateway.findByEmail(email);
		log.info("End email={}", email);
		return prepareUserToReturn(userOp);
	}

	private User prepareUserToReturn(Optional<User> userOp) {
		if(userOp.isEmpty()) {
			throw new UserNotFoundBusinessException();
		}
		return createUserWithoutPassword(userOp.get());
	}

	private User createUserWithoutPassword(User user) {
		return new User(user.getId(), user.getEmail(), user.getName(), null);
	}
}
