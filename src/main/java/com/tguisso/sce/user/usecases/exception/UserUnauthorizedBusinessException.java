package com.tguisso.sce.user.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class UserUnauthorizedBusinessException extends SceBaseException{
	private static final long serialVersionUID = -3693948226493032257L;

	private String code = "sce.userUnauthorized";
	private String message = "User unauthorized";
	private HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
}
