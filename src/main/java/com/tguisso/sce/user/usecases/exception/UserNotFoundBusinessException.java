package com.tguisso.sce.user.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class UserNotFoundBusinessException extends SceBaseException {
	private static final long serialVersionUID = 3272494276156103520L;

	private String code = "sce.user.userNotFound";
	private String message = "User not found";
	private HttpStatus httpStatus = HttpStatus.NOT_FOUND;
}
