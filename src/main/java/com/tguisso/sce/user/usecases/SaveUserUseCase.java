package com.tguisso.sce.user.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SaveUserUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway;
	
	@Autowired
	private SaveUserValidateUseCase saveUserValidateUseCase; 
	
	public String save(User user) {
		log.info("Start user={}", user);
		saveUserValidateUseCase.validate(user);
		log.info("End user={}", user);
		return userDBGateway.save(user);
	}
}
