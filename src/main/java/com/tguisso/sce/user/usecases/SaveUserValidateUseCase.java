package com.tguisso.sce.user.usecases;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.usecases.exception.UserAlreadyExistsBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SaveUserValidateUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway; 
	
	public void validate(User userToValidate) {
		log.info("Start userToValidate={}", userToValidate);
		Optional<User> userExistentOp = userDBGateway.findByEmail(userToValidate.getEmail());
		if(userExistentOp.isPresent() && !userExistentOp.get().getId().equals(userToValidate.getId())) {
			log.warn("User already exists: {}", userToValidate);
			throw new UserAlreadyExistsBusinessException();
		}
		log.info("End userToValidate={}", userToValidate);
	}
}
