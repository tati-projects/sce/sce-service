package com.tguisso.sce.user.usecases.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class UserAlreadyExistsBusinessException extends SceBaseException {
	private static final long serialVersionUID = -6961685326019261033L;

	private String code = "sce.userAlreadyExists";
	private String message = "User already exists";
	private HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
}
