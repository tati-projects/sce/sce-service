package com.tguisso.sce.user.gateways.stock.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ErrorToAccessStockServiceGatewayException extends SceBaseException {
	private static final long serialVersionUID = 5319557936908797256L;
	
	private String code = "sce.user.stock.errorToAccessStockService";
	private String message = "Error to access stock service";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

}
