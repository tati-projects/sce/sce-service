package com.tguisso.sce.user.gateways.database;

import java.util.Optional;

import com.tguisso.sce.user.domains.User;

public interface UserDBGateway {
	Optional<User> findByEmail(String email);
	Optional<User> findById(String id);
	String save(User user);
	void deleteById(String id);
}
