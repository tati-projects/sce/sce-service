package com.tguisso.sce.user.gateways.stock.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.stock.gateway.http.controllers.StockController;
import com.tguisso.sce.user.gateways.stock.StockGateway;
import com.tguisso.sce.user.gateways.stock.exception.ErrorToAccessStockServiceGatewayException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserDirectAccessToStock implements StockGateway{
	
	@Autowired
	private StockController stockController;

	@Override
	public void deleteStockByUserId(String id, String userName) {
		try {
			log.info("Start id={}, userName={}", id, userName);
			stockController.deleteByUser(id, userName);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			throw new ErrorToAccessStockServiceGatewayException();
		}
		
	}

}
