package com.tguisso.sce.user.gateways.http.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.http.controllers.json.UserJson;
import com.tguisso.sce.user.usecases.DeleteUserUseCase;
import com.tguisso.sce.user.usecases.GetUserUseCase;
import com.tguisso.sce.user.usecases.SaveUserUseCase;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("user")
public class UserController {
	
	@Autowired
	private SaveUserUseCase saveUserUseCase;
	
	@Autowired
	private GetUserUseCase getUserUseCase;
	
	@Autowired
	private DeleteUserUseCase deleteUserUseCase;
	
	@PostMapping
	public String create(@RequestBody(required = true) final UserJson userJson) {
		log.info("Start userJson={}", userJson);
		User userToSave = mapperJsonToUser(userJson);
		String userCreated = saveUserUseCase.save(userToSave);
		log.info("End userCreated={}", userCreated);
		return userCreated;
	}
	
	@PutMapping
	public void update(@RequestBody(required = true) final UserJson userJson) {
		log.info("Start userJson={}", userJson);
		User user = mapperJsonToUser(userJson);
		saveUserUseCase.save(user);
		log.info("End user={}", user);
	}
	
	@GetMapping("{id}")
	public UserJson get(@PathVariable(name = "id", required = true) final String id) {
		log.info("Start id={}", id);
		User user = getUserUseCase.get(id);
		log.info("End id={}", id);
		return mapperUserToJson(user);
	}
	
	@GetMapping("/email/{email}")
	public UserJson getByEmail(@PathVariable(name = "email", required = true) final String email) {
		log.info("Start email={}", email);
		User user = getUserUseCase.getByEmail(email);
		log.info("End user={}", user);
		return mapperUserToJson(user);
	}
	
	@DeleteMapping("{id}")
	public void delete(
			@PathVariable(name = "id", required = true) final String id,
			@RequestHeader(name = "logged-in-user-name", required = true)String userName){
		log.info("Start id={}, userName={}", id, userName);
		deleteUserUseCase.delete(id, userName);
		log.info("End id={}, userName={}", id, userName);
	}
	
	private User mapperJsonToUser(UserJson userJson) {
		return new User(userJson.getId(), userJson.getEmail(), userJson.getName(), userJson.getPassword());
	}
	
	private UserJson mapperUserToJson(User user) {
		return new UserJson(user.getId(), user.getEmail(), user.getName(), user.getPassword());
	}
}
