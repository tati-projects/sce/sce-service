package com.tguisso.sce.user.gateways.product.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.product.gateway.http.controllers.ProductController;
import com.tguisso.sce.user.gateways.product.UserProductGateway;
import com.tguisso.sce.user.gateways.product.exception.ErrorToAccessProductServiceGatewayException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserDirectAccessToProduct implements UserProductGateway{
	
	@Autowired
	private ProductController productController;

	@Override
	public void deleteProductByUserId(String userId, String userName) {
		try {
			log.info("Start userId={}, userName={}", userId, userName);
			productController.deleteByUser(userId, userName);
			log.info("End userId={}, userName={}", userId, userName);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			throw new ErrorToAccessProductServiceGatewayException();
		}
	}
}
