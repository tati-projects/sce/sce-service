package com.tguisso.sce.user.gateways.product.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ErrorToAccessProductServiceGatewayException extends SceBaseException {
	private static final long serialVersionUID = 4450769940538529088L;

	private String code = "sce.user.product.errorToAccessProductService";
	private String message = "Error to access product service";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
