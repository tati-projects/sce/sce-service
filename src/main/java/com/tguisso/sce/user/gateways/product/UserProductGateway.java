package com.tguisso.sce.user.gateways.product;

public interface UserProductGateway {
	void deleteProductByUserId(String userId, String userName);
}
