package com.tguisso.sce.user.gateways.database.mongo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.sce.user.domains.User;
import com.tguisso.sce.user.gateways.database.UserDBGateway;
import com.tguisso.sce.user.gateways.database.exception.ErrorToAccessDatabaseGatewayException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MongoDBGateway implements UserDBGateway {
	
	@Autowired
	private UserRepository userRepository; 
	
	@Override
	public Optional<User> findByEmail(String email) {
		try {
			log.info("Start email={}", email);
			Optional<User> userFoundOp = userRepository.findByEmail(email); 
			log.info("End userFoundOp={}", userFoundOp);
			return userFoundOp;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public String save(User user) {
		try {
			log.info("Start user={}", user);
			String generatedUserId = userRepository.save(user).getId(); 
			log.info("End generatedUserId={}", generatedUserId);
			return generatedUserId;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public Optional<User> findById(String id) {
		try {
			log.info("Start id={}", id);
			Optional<User> userFoundOp = userRepository.findById(id);
			log.info("End userFoundOp={}", userFoundOp);
			return userFoundOp;
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public void deleteById(String id) {
		try {
			log.info("Start id={}", id);
			userRepository.deleteById(id);
			log.info("End id={}", id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}		
	}

}
