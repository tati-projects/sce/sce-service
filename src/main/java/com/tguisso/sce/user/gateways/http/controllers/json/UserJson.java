package com.tguisso.sce.user.gateways.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserJson {
	private String id;
	private String email;
	private String name;
	private String password;
}
