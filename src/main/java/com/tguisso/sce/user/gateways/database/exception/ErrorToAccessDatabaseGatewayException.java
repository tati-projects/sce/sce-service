package com.tguisso.sce.user.gateways.database.exception;

import org.springframework.http.HttpStatus;

import com.tguisso.sce.starter.exception.base.SceBaseException;

import lombok.Getter;

@Getter
public class ErrorToAccessDatabaseGatewayException extends SceBaseException {
	private static final long serialVersionUID = -1220617552565604139L;
	
	private String code = "sce.errorToAccessDatabase";
	private String message = "Error to access database";
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
}
