package com.tguisso.sce.user.gateways.stock;

public interface StockGateway {
	void deleteStockByUserId(String id, String userName);
}
